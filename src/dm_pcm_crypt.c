/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "dm_pcm_crypt.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "dm_pcm.h"
#include "fs_utils.h"

/* OpenSSL */
#include <openssl/aes.h>
#include <openssl/sha.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <openssl/rand.h>
/* Std includes */
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <inttypes.h>
/* Sys includes */
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#define ME "pcm"
#define PCM_BUFFERSIZE 524288
size_t KEY_SIZE = 128;
#define KEY_SIZE_BYTES (KEY_SIZE / 8)
#define BASE64_KEY_SIZE_128 16
#define BASE64_KEY_SIZE_192 24
#define BASE64_KEY_SIZE_256 32

typedef struct {
    int fd;
    struct stat st;
    void* map;
} mmap_info;

typedef struct {
    unsigned char bytes[AES_BLOCK_SIZE];
} pcm_iv;

static bool file_map(const char* filename, mmap_info* info, void** ptr, size_t* size);
static bool file_unmap(mmap_info* info);
static bool pcm_base64_decode(unsigned char* in, size_t insize,
                              unsigned char** out, size_t* outsize);
static bool decode_key(const char* base64_key, unsigned char** deckey);
static bool pcm_encrypt_(unsigned char* in, size_t insize,
                         unsigned char* key, unsigned char** out, size_t* outsize);
static bool pcm_decrypt_(unsigned char* in, size_t insize, unsigned char* key,
                         unsigned char** out, size_t* outsize);
static bool iv_gen(pcm_iv* iv);
static bool hash_gen(unsigned char* data, size_t size, unsigned char* out_hash);
static bool buffer_encrypt(unsigned char* in, unsigned char* out, size_t size,
                           unsigned char* key, pcm_iv* iv);
static bool buffer_decrypt(unsigned char* in, unsigned char* out, size_t size,
                           unsigned char* key, pcm_iv* iv);
static bool write_to_file(const char* filename, const char* content, size_t content_size);


bool pcm_encrypt_dir(const char* path, const char* base64_key) {
    bool rv = false;
    amxc_llist_t* files = NULL;
    amxc_string_t encrypt_file;
    unsigned char* outbuf = NULL;

    when_str_empty_trace(path, exit, ERROR, "Invalid argument");
    when_str_empty_trace(base64_key, exit, ERROR, "Invalid argument");
    when_false_trace(directory_exist(path), exit, ERROR, "Directory %s does not exist", path);

    SAH_TRACEZ_INFO(ME, "Encrypt all files in the directory '%s' ", path);

    files = files_from_path(path);
    when_null_trace(files, exit, WARNING, "Directory: %s doesn't contain any file", path);

    amxc_llist_for_each(iter, files) {
        size_t outbufsize = 0;
        const char* file_str = NULL;
        const char* encrypt_file_str = NULL;
        amxc_string_t* file = amxc_container_of(iter, amxc_string_t, it);
        amxc_string_init(&encrypt_file, 0);

        //Ignore the files with suffix .encrypt
        if(strstr(amxc_string_get(file, 0), ".encrypt")) {
            amxc_string_clean(&encrypt_file);
            continue;
        }

        amxc_string_prependf(file, "%s/", path);
        amxc_string_copy(&encrypt_file, file);
        amxc_string_appendf(&encrypt_file, ".encrypt");

        encrypt_file_str = amxc_string_get(&encrypt_file, 0);
        file_str = amxc_string_get(file, 0);

        rv = pcm_encrypt_file(file_str, base64_key, &outbuf, &outbufsize);
        when_false_trace(rv, fail, ERROR, "Faild to encrypt the file: %s", file_str);

        rv = write_to_file(encrypt_file_str, (const char*) outbuf, outbufsize);
        when_false_trace(rv, fail, ERROR, "Could not write on the %s", encrypt_file_str);

        if(remove(file_str) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to delete %s", file_str);
        }

        amxc_string_clean(&encrypt_file);
        outbufsize = 0;
        free(outbuf);
        outbuf = NULL;
    }

fail:
    free(outbuf);
    amxc_string_clean(&encrypt_file);
    amxc_llist_delete(&files, amxc_string_list_it_free);
exit:
    return rv;
}

bool pcm_decrypt_dir(const char* path, const char* base64_key) {
    bool rv = false;
    amxc_llist_t* files = NULL;
    amxc_string_t decrypt_file;
    unsigned char* outbuf = NULL;

    when_str_empty_trace(path, exit, ERROR, "Invalid argument");
    when_str_empty_trace(base64_key, exit, ERROR, "Invalid argument");
    when_false_trace(directory_exist(path), exit, ERROR, "Directory %s does not exist", path);

    SAH_TRACEZ_INFO(ME, "Encrypt all files in the directory '%s' ", path);

    files = files_from_path(path);
    when_null_trace(files, exit, WARNING, "Directory: %s doesn't contain any file", path);

    amxc_llist_for_each(iter, files) {
        size_t outbufsize = 0;
        const char* file_str = NULL;
        const char* decrypt_file_str = NULL;
        amxc_string_t* file = amxc_container_of(iter, amxc_string_t, it);
        amxc_string_init(&decrypt_file, 0);

        //Accept only the files with suffix .encrypt
        if(strstr(amxc_string_get(file, 0), ".encrypt") == NULL) {
            amxc_string_clean(&decrypt_file);
            continue;
        }

        amxc_string_prependf(file, "%s/", path);
        amxc_string_copy(&decrypt_file, file);
        amxc_string_replace(&decrypt_file, ".encrypt", "", 1);

        file_str = amxc_string_get(file, 0);
        decrypt_file_str = amxc_string_get(&decrypt_file, 0);

        rv = pcm_decrypt_file(file_str, base64_key, &outbuf, &outbufsize);
        when_false_trace(rv, fail, ERROR, "Faild to decrypt the file: %s", file_str);

        rv = write_to_file(decrypt_file_str, (const char*) outbuf, outbufsize);
        when_false_trace(rv, fail, ERROR, "Could not write on the %s", decrypt_file_str);

        if(remove(file_str) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to delete %s", file_str);
        }

        amxc_string_clean(&decrypt_file);
        outbufsize = 0;
        free(outbuf);
        outbuf = NULL;
    }

fail:
    free(outbuf);
    amxc_string_clean(&decrypt_file);
    amxc_llist_delete(&files, amxc_string_list_it_free);

exit:
    return rv;
}

bool pcm_encrypt_file(const char* filename, const char* base64_key,
                      unsigned char** out, size_t* outsize) {
    bool rv = false;
    mmap_info mi;
    void* file_content;
    size_t file_size;

    if(!file_map(filename, &mi, &file_content, &file_size)) {
        SAH_TRACEZ_ERROR(ME, "Failed to open file \"%s\"", filename);
        goto exit;
    }
    SAH_TRACEZ_INFO(ME, "File opened successfully (size %zu)", file_size);
    rv = pcm_encrypt(file_content, file_size, base64_key, out, outsize);
    file_unmap(&mi);

exit:
    return rv;

}

bool pcm_decrypt_file(const char* filename, const char* base64_key,
                      unsigned char** out, size_t* outsize) {
    bool rv = false;
    mmap_info mi;
    void* file_content;
    size_t file_size;

    if(!file_map(filename, &mi, &file_content, &file_size)) {
        SAH_TRACEZ_ERROR(ME, "Failed to open file \"%s\"", filename);
        goto exit;
    }
    SAH_TRACEZ_INFO(ME, "File opened successfully (size %zu)", file_size);
    rv = pcm_decrypt(file_content, file_size, base64_key, out, outsize);
    file_unmap(&mi);

exit:
    return rv;

}

bool pcm_encrypt(unsigned char* in, size_t insize, const char* base64_key,
                 unsigned char** out, size_t* outsize) {
    bool rv = false;
    bool needToFree = false;
    unsigned char* dec = NULL;
    size_t decsize;
    unsigned char* deckey = NULL;

    if(insize > PCM_BUFFERSIZE) {
        SAH_TRACEZ_ERROR(ME, "File size exceeds %iK!", PCM_BUFFERSIZE / 1024);
        goto exit;
    }

    when_false_trace(decode_key(base64_key, &deckey), exit, ERROR, "Failed to decode the BASE64 key.");

    if(insize % 16 != 0) {
        decsize = (insize / 16 + 1) * 16;
        dec = calloc(1, decsize);
        when_null_trace(dec, exit, ERROR, "Alloc failed");
        memcpy(dec, in, insize);
        needToFree = true;
    } else {
        decsize = insize;
        dec = in;
    }

    rv = pcm_encrypt_(dec, decsize, deckey, out, outsize);

exit:
    if(needToFree) {
        free(dec);
    }
    free(deckey);
    return rv;
}

bool pcm_decrypt(unsigned char* in, size_t insize, const char* base64_key,
                 unsigned char** out, size_t* outsize) {
    bool rv = false;
    unsigned char* deckey = NULL;

    when_false_trace(decode_key(base64_key, &deckey), exit, ERROR, "Failed to decode the BASE64 key.");

    rv = pcm_decrypt_(in, insize, deckey, out, outsize);
    when_false_trace(rv, exit, ERROR, "Faild to encrypt the input");

exit:
    free(deckey);
    return rv;
}

/** Map a file into memory.
 *
 * @param      filename
 * @param[out] info mmap helper struct to fill
 * @param[out] ptr  data pointer
 * @param[out] size data size
 * @return true if success, false if error
 */
static bool file_map(const char* filename, mmap_info* info, void** ptr, size_t* size) {
    bool rv = false;
    mmap_info i = {.fd = -1, .map = NULL};

    when_null(info, exit);
    when_null(ptr, exit);
    when_null(size, exit);

    i.fd = open(filename, O_RDONLY);
    when_true_trace(i.fd == -1, exit, ERROR, "Failed to open \"%s\"", filename);

    when_true_trace(fstat(i.fd, &i.st) == -1, fail, ERROR, "Failed to get the size of \"%s\"", filename);

    i.map = mmap(NULL, i.st.st_size, PROT_READ, MAP_PRIVATE, i.fd, 0);
    when_true_trace(i.map == MAP_FAILED, fail, ERROR, "Failed to map \"%s\" of size %" PRIi64,
                    filename, (int64_t) i.st.st_size);

    memcpy(info, &i, sizeof(*info));
    *ptr = i.map;
    *size = i.st.st_size;
    rv = true;
    goto exit;

fail:
    file_unmap(&i);
exit:
    return rv;
}

/** Unmap a file.
 *
 * @param info mmap helper struct
 * @return true if success, false if error
 */
static bool file_unmap(mmap_info* info) {
    bool rv = false;

    when_null(info, exit);

    if((info->map != MAP_FAILED) && (info->map != NULL)) {
        munmap(info->map, info->st.st_size);
    }
    if(info->fd != -1) {
        close(info->fd);
    }
    rv = true;

exit:
    return rv;
}

/** Decode a base64 buffer into binary.
 *
 * @param      in      input data
 * @param      insize  size of input data
 * @param[out] out     output data
 * @param[out] outsize size of output data
 * @return true if success, false if error
 */
static bool pcm_base64_decode(unsigned char* in, size_t insize,
                              unsigned char** out, size_t* outsize) {
    BIO* bio_64 = NULL;
    BIO* bio_mem = NULL;
    unsigned char* outbuf = NULL;
    size_t outbufsize = 0;
    bool rv = false;
    int num_bytes;

    when_null(in, exit);
    when_null(out, exit);
    when_null(outsize, exit);

    bio_64 = BIO_new(BIO_f_base64());
    when_null(bio_64, exit);
    BIO_set_flags(bio_64, BIO_FLAGS_BASE64_NO_NL);

    bio_mem = BIO_new_mem_buf(in, insize);
    when_null(bio_mem, exit);

    BIO_push(bio_64, bio_mem);

    outbufsize = insize;
    outbuf = calloc(1, outbufsize);
    when_null_trace(outbuf, exit, ERROR, "Alloc failed");
    num_bytes = BIO_read(bio_64, outbuf, outbufsize);
    when_true(num_bytes <= 0, exit);

    outbufsize = num_bytes;
    outbuf[outbufsize] = '\0';
    outbuf = realloc(outbuf, outbufsize); // shrink buffer

    *out = outbuf;
    *outsize = outbufsize;
    rv = true;

exit:
    if(bio_mem) {
        BIO_free(bio_mem);
    }
    if(bio_64) {
        BIO_free(bio_64);
    }
    return rv;
}

/** Decode a base64 key and verify whether the key is valid for use
 *  with the AES encryption algorithm.
 *
 * @param      base64_key   base64-encoded key
 * @param[out] deckey decoded key
 * @return true if the key is valid.
 */
static bool decode_key(const char* base64_key, unsigned char** deckey) {
    bool rv = false;
    size_t dec_size = 0;

    when_null(base64_key, exit);
    when_null(deckey, exit);

    rv = pcm_base64_decode((unsigned char*) base64_key, strlen(base64_key), deckey, &dec_size);
    when_false(rv, exit);
    switch(dec_size) {
    case BASE64_KEY_SIZE_128: KEY_SIZE = 128; break;
    case BASE64_KEY_SIZE_192: KEY_SIZE = 192; break;
    case BASE64_KEY_SIZE_256: KEY_SIZE = 256; break;
    default:
        SAH_TRACEZ_ERROR(ME, "Invalid key for the AES encryption algorithm");
        rv = false;
        goto exit;
    }
exit:
    return rv;
}

/** Encrypt an input buffer into the full raw encrypted format.
 *
 * The full raw encrypted format is the following:
 * [      SHA1 hash       ] (20 bytes)
 * [     SHA1 key hash    ] (20 bytes)
 * [ Initialization Vector] (16 bytes)
 * [        Data          ]
 *
 * The SHA1 hash is the hash of IV+Data.
 * @note the input data size MUST be a multiple of 16!
 *
 * @param      in       input data
 * @param      insize   size of input data
 * @param      key      key to use for encryption
 * @param[out] out      output data
 * @param[out] outsize  size of output data
 * @return true if success, false if error
 */
static bool pcm_encrypt_(unsigned char* in, size_t insize, unsigned char* key,
                         unsigned char** out, size_t* outsize) {
    bool rv = false;
    pcm_iv iv;
    unsigned char* outbuf = NULL;
    unsigned char* keyhptr = NULL;
    unsigned char* ivptr = NULL;
    unsigned char* encptr = NULL;
    size_t outbufsize = 0;
    size_t keyhptrsize = 0;

    when_null(out, exit);
    when_null(outsize, exit);

    rv = iv_gen(&iv);
    when_false_trace(rv, exit, ERROR, "Failed to generate IV!");

    outbufsize = 2 * SHA_DIGEST_LENGTH + AES_BLOCK_SIZE + insize;
    outbuf = calloc(1, outbufsize);
    when_null_trace(outbuf, exit, ERROR, "Alloc failed");

    keyhptr = outbuf + SHA_DIGEST_LENGTH;
    keyhptrsize = outbufsize - SHA_DIGEST_LENGTH;

    ivptr = keyhptr + SHA_DIGEST_LENGTH;

    encptr = ivptr + AES_BLOCK_SIZE;

    rv = buffer_encrypt(in, encptr, insize, key, &iv);
    when_false_trace(rv, fail, ERROR, "Failed to encrypt!");

    memcpy(ivptr, iv.bytes, AES_BLOCK_SIZE);

    rv = hash_gen(key, KEY_SIZE_BYTES, keyhptr);
    when_false_trace(rv, fail, ERROR, "Failed to generate the key hash!");

    /* Generate the hash for [Key hash + IV + encrypted data] */
    SAH_TRACEZ_INFO(ME, "Hash on data of size %zu", keyhptrsize);
    rv = hash_gen(keyhptr, keyhptrsize, outbuf);
    when_false_trace(rv, fail, ERROR, "Failed to generate the hash!");

    SAH_TRACEZ_INFO(ME, "Hash: %02x%02x%02x%02x"
                    "%02x%02x%02x%02x"
                    "%02x%02x%02x%02x"
                    "%02x%02x%02x%02x"
                    "%02x%02x%02x%02x",
                    outbuf[ 0], outbuf[ 1], outbuf[ 2], outbuf[ 3],
                    outbuf[ 4], outbuf[ 5], outbuf[ 6], outbuf[ 7],
                    outbuf[ 8], outbuf[ 9], outbuf[10], outbuf[11],
                    outbuf[12], outbuf[13], outbuf[14], outbuf[15],
                    outbuf[16], outbuf[17], outbuf[18], outbuf[19]);

    *outsize = outbufsize;
    *out = outbuf;
    rv = true;
    goto exit;

fail:
    free(outbuf);
exit:
    return rv;
}

/** Decrypt an input buffer (full raw encrypted format).
 *
 * @see hgwcfg_encrypt for the format description.
 *
 * @param      in       input data
 * @param      insize   size of input data
 * @param      key      key to use for decryption
 * @param[out] out      output data
 * @param[out] outsize  size of output data
 * @return true if success, false if error
 */
static bool pcm_decrypt_(unsigned char* in, size_t insize, unsigned char* key,
                         unsigned char** out, size_t* outsize) {
    bool rv = false;
    pcm_iv iv;
    unsigned char hash[SHA_DIGEST_LENGTH];
    unsigned char* keyhptr = NULL;
    unsigned char* ivptr = NULL;
    unsigned char* encptr = NULL;
    unsigned char* outbuf = NULL;
    size_t keyhptrsize = 0;
    size_t ivptrsize = 0;
    size_t encptrsize = 0;
    size_t outbufsize = 0;

    when_null(out, exit);
    when_null(outsize, exit);

    if(insize < 2 * SHA_DIGEST_LENGTH + AES_BLOCK_SIZE) {
        SAH_TRACEZ_ERROR(ME, "Invalid input size of %zu!", insize);
        goto exit;
    }

    keyhptr = in + SHA_DIGEST_LENGTH;
    keyhptrsize = insize - SHA_DIGEST_LENGTH;
    ivptr = keyhptr + SHA_DIGEST_LENGTH;
    ivptrsize = keyhptrsize - SHA_DIGEST_LENGTH;
    encptr = ivptr + AES_BLOCK_SIZE;
    encptrsize = ivptrsize - AES_BLOCK_SIZE;

    /* Generate the hash for [Key hash + IV + encrypted data] */
    SAH_TRACEZ_INFO(ME, "Hash on data of size %zu", keyhptrsize);
    rv = hash_gen(keyhptr, keyhptrsize, hash);
    when_false_trace(rv, exit, ERROR, "Failed to generate the hash!");
    SAH_TRACEZ_INFO(ME, "Hash: %02x%02x%02x%02x"
                    "%02x%02x%02x%02x"
                    "%02x%02x%02x%02x"
                    "%02x%02x%02x%02x"
                    "%02x%02x%02x%02x",
                    hash[ 0], hash[ 1], hash[ 2], hash[ 3],
                    hash[ 4], hash[ 5], hash[ 6], hash[ 7],
                    hash[ 8], hash[ 9], hash[10], hash[11],
                    hash[12], hash[13], hash[14], hash[15],
                    hash[16], hash[17], hash[18], hash[19]);
    if(memcmp(hash, in, SHA_DIGEST_LENGTH) != 0) {
        SAH_TRACEZ_ERROR(ME, "Hash check failed!");
        goto exit;
    }

    memcpy(iv.bytes, ivptr, AES_BLOCK_SIZE);

    if(!hash_gen(key, KEY_SIZE_BYTES, hash)) {
        SAH_TRACEZ_ERROR(ME, "Failed to generate the key hash!");
        goto exit;
    }

    if(memcmp(hash, keyhptr, SHA_DIGEST_LENGTH) != 0) {
        SAH_TRACEZ_ERROR(ME, "Key hash check failed!");
        goto exit;
    }
    SAH_TRACEZ_INFO(ME, "Key hash checked OK");

    outbufsize = encptrsize;
    outbuf = calloc(1, outbufsize);
    when_null_trace(outbuf, exit, ERROR, "Alloc failed");

    rv = buffer_decrypt(encptr, outbuf, encptrsize, key, &iv);
    when_false_trace(rv, fail, ERROR, "Failed to decrypt!");

    *outsize = outbufsize;
    *out = outbuf;
    rv = true;
    goto exit;

fail:
    free(outbuf);
exit:
    return rv;
}

/** Generate an initialization vector with random data.
 *
 * @param[inout] iv Initialization Vector to fill
 * @return true if success, false if error
 */
static bool iv_gen(pcm_iv* iv) {
    if(iv == NULL) {
        return false;
    }
    return RAND_bytes(iv->bytes, AES_BLOCK_SIZE) != 0;
}

/** Generate a (SHA1) hash of the input data.
 *
 * @note out_hash must be a pointer to a buffer of size SHA_DIGEST_LENGTH
 *
 * @param data data buffer
 * @param size size of the data buffer
 * @param[out] out_hash buffer to fill with the hash
 * @return true if success, false if error
 */
static bool hash_gen(unsigned char* data, size_t size, unsigned char* out_hash) {
    bool rv = false;
    const EVP_MD* md = EVP_sha1();
    EVP_MD_CTX* mdctx = EVP_MD_CTX_new();
    unsigned int md_len;

    when_null(mdctx, exit);
    when_false(EVP_DigestInit_ex(mdctx, md, NULL), clean);
    when_false(EVP_DigestUpdate(mdctx, data, size), clean);
    when_false(EVP_DigestFinal_ex(mdctx, out_hash, &md_len), clean);

    rv = true;
clean:
    EVP_MD_CTX_free(mdctx);

exit:
    return rv;
}

/** Encrypt the buffer with the given IV and Key.
 *
 * @note the size of the input buffer MUST be a multiple of 16!
 * @note the size of the output buffer must be the same than the input buffer
 *
 * @param      in   input buffer
 * @param[out] out  output buffer to fill
 * @param      size size of input buffer
 * @param      key  Key to use
 * @param      iv   Initialization Vector to use
 * @return true if success, false if error
 */
static bool buffer_encrypt(unsigned char* in, unsigned char* out, size_t size,
                           unsigned char* key, pcm_iv* iv) {
    bool rv = false;
    int outlen, outlenfinal;

    EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
    when_null(ctx, exit);

    when_null(in, clean);
    when_null(out, clean);
    when_null(key, clean);
    when_null(iv, clean);

    memset(out, 0x00, size);
    when_false(EVP_EncryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, key, iv->bytes), clean);
    when_false(EVP_CIPHER_CTX_set_padding(ctx, 0), clean);
    when_false(EVP_EncryptUpdate(ctx, out, &outlen, in, size), clean);
    when_false(EVP_EncryptFinal_ex(ctx, out + outlen, &outlenfinal), clean);

    rv = true;
clean:
    EVP_CIPHER_CTX_free(ctx);
exit:
    return rv;
}

/** Decrypt the buffer with the given IV and key.
 *
 * @note the size of the output buffer must be the same than the input buffer
 *
 * @param      in   input buffer
 * @param[out] out  output buffer to fill
 * @param      size size of input buffer
 * @param      key  Key to use
 * @param      iv   Initialization Vector to use
 * @return true if success, false if error
 */
static bool buffer_decrypt(unsigned char* in, unsigned char* out, size_t size,
                           unsigned char* key, pcm_iv* iv) {
    bool rv = false;
    int outlen, outlenfinal;

    EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
    when_null(ctx, exit);

    when_null(in, clean);
    when_null(out, clean);
    when_null(key, clean);
    when_null(iv, clean);

    memset(out, 0x00, size);
    when_false(EVP_DecryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, key, iv->bytes), clean);
    when_false(EVP_CIPHER_CTX_set_padding(ctx, 0), clean);
    when_false(EVP_DecryptUpdate(ctx, out, &outlen, in, size), clean);
    when_false(EVP_DecryptFinal_ex(ctx, out + outlen, &outlenfinal), clean);

    rv = true;
clean:
    EVP_CIPHER_CTX_free(ctx);
exit:
    return rv;
}

/** Writes the content of a given character array to a file with the specified name.
 *  The size of the content to be written needs to be specified. If the file does not exist,
 *  it will be created. If the file already exists, its contents will be truncated.
 *
 * @param  filename       Name of the file to be wtitten
 * @param  content        Content to be written
 * @param  content_size   Size of the content
 * @return true if success, false if error
 */
static bool write_to_file(const char* filename, const char* content, size_t content_size) {
    bool rv = false;
    int fd = -1;
    int num_bytes_written = -1;

    when_str_empty_trace(filename, exit, ERROR, "Invalid argument");
    when_null_trace(content, exit, ERROR, "Invalid argument");

    fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0644);
    when_true_trace(fd == -1, exit, ERROR, "Can not open the file: %s", filename);
    num_bytes_written = write(fd, content, content_size);
    when_true_trace(num_bytes_written == -1, fail, ERROR, "Can not write to file");
    rv = true;

fail:
    close(fd);
exit:
    return rv;
}
#undef ME
