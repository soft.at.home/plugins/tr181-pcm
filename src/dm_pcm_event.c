/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dm_pcm.h"
#include "dm_pcm_event.h"
#include "dm_pcm_service.h"
#include "fs_utils.h"

#define ME "pcm"

/**
 * @brief
 * Updates the value of PersistentConfiguration.Controller based on the backup_file_type argument provided.
 * If the `backup_file_type` is specified as "JSON", the controller is set to "mod-pcm-json".
 * If the argument is "XML", the controller is adjusted to "mod-pcm-xml".
 *
 * @param backup_file_type Type of the backup file [JSON, XML]
 * @return amxd_status_ok when updated successfully, otherwise an other error code and no changes in the data model are done.
 */
static void set_controller(const char* backup_file_type) {
    amxd_trans_t transaction;
    amxd_trans_init(&transaction);

    when_null(backup_file_type, exit);

    amxd_trans_select_pathf(&transaction, "%s", pcm_root_object());
    amxd_trans_set_attr(&transaction, amxd_tattr_change_prot, true);
    if(strcmp(backup_file_type, "JSON") == 0) {
        amxd_trans_set_value(cstring_t, &transaction, "Controller", "mod-pcm-json");
    }
    if(strcmp(backup_file_type, "XML") == 0) {
        amxd_trans_set_value(cstring_t, &transaction, "Controller", "mod-pcm-xml");
    }
    when_failed_trace(amxd_trans_apply(&transaction, pcm_get_dm()), exit, ERROR, "Failed to apply the transaction");

exit:
    amxd_trans_clean(&transaction);
    return;
}

/**
 * @brief
 * This function removes the initial instance of
 * the `PersistentConfiguration.BackupFile.` template object with a specified tag value.
 *
 * @param tag Tag of the backup file. [Manual, Dynamic, Upload]
 * @param object pointer to the data model object PersistentConfiguration.BackupFile.
 * @return amxd_status_ok when deleted successfully, otherwise an other error code and no changes in the data model are done.
 */
static amxd_status_t delete_first_backup_type(const char* tag, amxd_object_t* object) {
    amxd_status_t ret = amxd_status_unknown_error;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    when_null_trace(object, exit, ERROR, "Object argument is NULL");
    when_str_empty_trace(tag, exit, ERROR, "The Tag value is not defined; [Manual, Dynamic, Upload]");

    ret = amxd_trans_select_object(&trans, object);
    when_failed_trace(ret, exit, ERROR, "The object : %s can not be selected", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    amxd_object_for_each(instance, it, object) {
        amxd_object_t* inst = amxc_container_of(it, amxd_object_t, it);
        char* inst_tag = amxd_object_get_value(cstring_t, inst, "Tag", NULL);
        uint32_t index = amxd_object_get_index(inst);
        if((inst_tag != NULL) && (strcmp(inst_tag, tag) == 0)) {
            amxd_trans_del_inst(&trans, index, NULL);
            free(inst_tag);
            break;
        }
        free(inst_tag);
    }
    ret = amxd_trans_apply(&trans, pcm_get_dm());
    when_failed_trace(ret, exit, ERROR, "Failed to apply the transaction");

exit:
    amxd_trans_clean(&trans);
    return ret;
}

/**
 * @brief
 * This function is responsible for evaluating whether backup files with a specified tag have surpassed their
 * respective limits. If the tag is "Upload" or "Manual" the allowable limit is one file. However,
 * for the "Dynamic" tag, the limit is determined by the value of the `MaxNumberOfDynamicFiles` parameter.
 *
 * @param tag Tag of the backup file. [Manual, Dynamic, Upload]
 * @return `True` if the BackupFile instances are exceed their limits, otherwise returns `False`.
 */
static bool file_type_reached_limit(const char* tag) {
    bool ret = false;
    amxd_object_t* root_obj = NULL;
    amxd_object_t* backup_file_obj = NULL;
    uint32_t max_backup_number = 0;
    uint32_t number_of_entries = 0;

    backup_file_obj = amxd_dm_findf(pcm_get_dm(), "%s.BackupFile.", pcm_root_object());
    when_null_trace(backup_file_obj, exit, ERROR, "Object %s.BackupFile. not found", pcm_root_object());

    root_obj = amxd_dm_findf(pcm_get_dm(), "%s", pcm_root_object());
    when_null_trace(root_obj, exit, ERROR, "Object %s not found", pcm_root_object());

    if(strcmp(tag, "Dynamic") == 0) {
        max_backup_number = amxd_object_get_value(uint32_t, root_obj, "MaxNumberOfDynamicFiles", NULL);
        number_of_entries = amxd_object_get_value(uint32_t, root_obj, "NumberOfDynamicFiles", NULL);
    } else {
        max_backup_number = 1;
        number_of_entries = 0;

        amxd_object_for_each(instance, it, backup_file_obj) {
            amxd_object_t* inst = amxc_container_of(it, amxd_object_t, it);
            char* inst_tag = amxd_object_get_value(cstring_t, inst, "Tag", NULL);
            if((inst_tag != NULL) && (strcmp(inst_tag, tag) == 0)) {
                number_of_entries++;
            }
            free(inst_tag);
        }
    }

    ret = (number_of_entries > max_backup_number);
exit:
    return ret;
}

/**
 * @brief
 * Updates the PCM Controller acording to the Backup File type.
 * Called when the PersistentConfiguration.Config.BackupFileType
 * changed between JSON and XML.
 *
 * @param event_name event name
 * @param event_data event data
 * @param priv private data associated with event subscription
 */
void _pcm_backup_file_type_changed(UNUSED const char* const event_name,
                                   const amxc_var_t* const event_data,
                                   UNUSED void* const priv) {
    const amxd_dm_t* dm = NULL;
    const char* backup_fle_type = NULL;

    SAH_TRACEZ_INFO(ME, "PCM config BackupFileType has been changed in TR-181 data model");
    when_null_trace(event_data, exit, ERROR, "Invalid event_data argument");
    dm = pcm_get_dm();
    when_null_trace(dm, exit, ERROR, "Cannot get PCM data model.");

    backup_fle_type = GETP_CHAR(event_data, "parameters.BackupFileType.to");
    set_controller(backup_fle_type);

exit:
    return;
}

/**
 * @brief
 * Align the instances of BackupFile in accordance with the value of MaxNumberOfDynamicFiles.
 * If the number of instances exceeds the specified MaxNumberOfDynamicFiles value, it shall be responsible
 * for the removal of the oldest instances.
 *
 * @param event_name event name
 * @param event_data event data
 * @param priv private data associated with event subscription
 */
void _max_number_dynamic_files_changed(UNUSED const char* const event_name,
                                       const amxc_var_t* const event_data,
                                       UNUSED void* const priv) {
    amxd_object_t* backup_file_obj = NULL;
    amxd_object_t* root_obj = NULL;
    uint32_t number_of_entries = 0;
    uint32_t max_backup_number = GETP_UINT32(event_data, "parameters.MaxNumberOfDynamicFiles.to");

    backup_file_obj = amxd_dm_findf(pcm_get_dm(), "%s.BackupFile.", pcm_root_object());
    when_null_trace(backup_file_obj, exit, ERROR, "Object %s.BackupFile. not found", pcm_root_object());

    root_obj = amxd_dm_findf(pcm_get_dm(), "%s", pcm_root_object());
    when_null_trace(root_obj, exit, ERROR, "Object %s not found", pcm_root_object());

    number_of_entries = amxd_object_get_value(uint32_t, root_obj, "NumberOfDynamicFiles", NULL);

    while(number_of_entries > max_backup_number) {
        delete_first_backup_type("Dynamic", backup_file_obj);
        number_of_entries--;
    }

exit:
    return;
}

/**
 * @brief
 * This function is invoked each time a new instance of BackupFile is added.
 * If the added instance has the "Dynamic" tag, it will increment the NumberOfDynamicFiles
 * by one. For each type of tag, it performs a validation check on the new addition to the
 * BackupFile. Specifically, for "Dynamic" backup files, it enforces a limit based on
 * MaxNumberOfDynamicFiles, while for "Upload" and "Manual" tags, the limit is hardcoded to one instance.
 *
 * @param event_name event name
 * @param event_data event data
 * @param priv private data associated with event subscription
 */
void _backup_file_added(UNUSED const char* const event_name,
                        const amxc_var_t* const event_data,
                        UNUSED void* const priv) {
    amxd_object_t* root_obj = NULL;
    amxd_object_t* backup_file_obj = NULL;
    const char* tag = NULL;
    uint32_t num_backup_files = 0;

    tag = GETP_CHAR(event_data, "parameters.Tag");
    when_str_empty_trace(tag, exit, ERROR, "The Tag value is not defined; [Manual, Dynamic, Upload]");

    backup_file_obj = amxd_dm_findf(pcm_get_dm(), "%s.BackupFile.", pcm_root_object());
    when_null_trace(backup_file_obj, exit, ERROR, "Object %s.BackupFile. not found", pcm_root_object());

    root_obj = amxd_dm_findf(pcm_get_dm(), "%s", pcm_root_object());
    when_null_trace(root_obj, exit, ERROR, "Object %s not found", pcm_root_object());
    num_backup_files = amxd_object_get_value(uint32_t, root_obj, "NumberOfDynamicFiles", NULL);

    if(strcmp(tag, "Dynamic") == 0) {
        amxd_status_t ret = amxd_status_unknown_error;
        amxd_trans_t trans;
        amxd_trans_init(&trans);

        ret = amxd_trans_select_object(&trans, root_obj);
        when_failed_trace(ret, exit, ERROR, "The object : %s can not be selected", amxd_object_get_name(root_obj, AMXD_OBJECT_NAMED));

        amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
        amxd_trans_set_value(uint32_t, &trans, "NumberOfDynamicFiles", ++num_backup_files);
        ret = amxd_trans_apply(&trans, pcm_get_dm());

        if(ret != amxd_status_ok) {
            amxd_trans_clean(&trans);
            SAH_TRACEZ_ERROR(ME, "Failed to apply the transaction");
            goto exit;
        }

        amxd_trans_clean(&trans);
    }

    if(file_type_reached_limit(tag)) {
        delete_first_backup_type(tag, backup_file_obj);
    }

exit:
    return;
}

/**
 * @brief
 * This function is invoked each time a new instance of BackupFile is removed.
 * If the removed instance has the "Dynamic" tag, it will decrement the NumberOfDynamicFiles
 * by one. For each type of tag, it removes the file
 *
 * @param event_name event name
 * @param event_data event data
 * @param priv private data associated with event subscription
 */
void _backup_file_removed(UNUSED const char* const event_name,
                          const amxc_var_t* const event_data,
                          UNUSED void* const priv) {
    amxd_object_t* root_obj = NULL;
    uint32_t num_backup_files = 0;
    const char* filename = GETP_CHAR(event_data, "parameters.FileName");
    const char* tag = GETP_CHAR(event_data, "parameters.Tag");

    when_str_empty_trace(tag, exit, ERROR, "The Tag value is not defined; [Manual, Dynamic, Upload]");

    root_obj = amxd_dm_findf(pcm_get_dm(), "%s", pcm_root_object());
    when_null_trace(root_obj, exit, ERROR, "Object %s not found", pcm_root_object());
    num_backup_files = amxd_object_get_value(uint32_t, root_obj, "NumberOfDynamicFiles", NULL);

    if(strcmp(tag, "Dynamic") == 0) {
        amxd_status_t ret = amxd_status_unknown_error;
        amxd_trans_t trans;
        amxd_trans_init(&trans);

        ret = amxd_trans_select_object(&trans, root_obj);
        when_failed_trace(ret, exit, ERROR, "The object : %s can not be selected", amxd_object_get_name(root_obj, AMXD_OBJECT_NAMED));

        amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
        amxd_trans_set_value(uint32_t, &trans, "NumberOfDynamicFiles", --num_backup_files);
        ret = amxd_trans_apply(&trans, pcm_get_dm());

        if(ret != amxd_status_ok) {
            amxd_trans_clean(&trans);
            SAH_TRACEZ_ERROR(ME, "Failed to apply the transaction");
            goto exit;
        }

        amxd_trans_clean(&trans);
    }

    remove_file(pcm_get_store_path("export"), filename);

exit:
    return;
}

#undef ME