/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <fcntl.h>
#include <stdio.h>
#include <errno.h>

#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <debug/sahtrace.h>
#include "fs_utils.h"
#include <dirent.h>

#if defined(CONFIG_SAH_AMX_PCM_TARBALL) && CONFIG_SAH_AMX_PCM_TARBALL
#include <archive.h>
#include <archive_entry.h>
#define archive_err_str(a) (archive_error_string((a)) ? : "unknown error")
/* some target compilation fail on O_CLOEXEC symbol */
#ifndef O_CLOEXEC
#define O_CLOEXEC 0
#endif
#endif

#ifdef ME
#undef ME
#endif

#define ME "pcm"

amxc_llist_t* files_from_path(const char* path) {
    int retval = -1;
    DIR* d = NULL;
    struct dirent* dir = NULL;
    amxc_llist_t* directories = NULL;

    when_str_empty_trace(path, exit, ERROR, "Empty directory path");
    when_false_trace(directory_exist(path), exit, ERROR, "Directory %s does not exist", path);
    when_failed_trace(amxc_llist_new(&directories), exit, ERROR, "Cannot create file list");

    d = opendir(path);
    when_null_trace(d, exit, ERROR, "Unable to read directory %s", path);

    while((dir = readdir(d)) != NULL) {
        if((strstr(dir->d_name, ".json") != NULL) || (strstr(dir->d_name, ".xml") != NULL) ||
           (strstr(dir->d_name, ".encrypt") != NULL)) {
            amxc_llist_add_string(directories, dir->d_name);
        }
    }

    closedir(d);

    when_true_trace(amxc_llist_is_empty(directories), exit, ERROR, "Empty directory %s", path);

    retval = 0;

exit:
    if((retval != 0) && (directories != NULL)) {
        amxc_llist_delete(&directories, amxc_string_list_it_free);
    }
    return directories;
}

bool directory_exist(const char* path) {
    struct stat path_stat;
    bool exist = false;
    memset(&path_stat, 0, sizeof(struct stat));

    when_str_empty(path, exit);
    when_true((-1 == stat(path, &path_stat)), exit);

    exist = S_ISDIR(path_stat.st_mode);

exit:
    return exist;
}

bool file_exist(const char* filename) {
    struct stat file_stat;
    bool exist = false;
    memset(&file_stat, 0, sizeof(struct stat));

    when_str_empty(filename, exit);
    exist = (stat(filename, &file_stat) != -1);

exit:
    return exist;
}

int remove_file(const char* path, const char* filename) {
    int rc = -1;
    amxc_string_t fullpath;
    const char* fullpath_str = NULL;

    amxc_string_init(&fullpath, 0);

    when_str_empty_trace(filename, exit, ERROR, "filename is empty");
    when_str_empty_trace(path, exit, ERROR, "path is empty");

    when_false_trace(directory_exist(path), exit, ERROR, "Directory does not exist");
    amxc_string_setf(&fullpath, "%s/%s", path, filename);
    fullpath_str = amxc_string_get(&fullpath, 0);
    when_false_trace(file_exist(fullpath_str), exit, WARNING, "File does not exist: %s", fullpath_str);

    rc = remove(fullpath_str);
exit:
    amxc_string_clean(&fullpath);
    return rc;
}

amxd_status_t mkdir_p(const char* path) {
    amxd_status_t rc = amxd_status_unknown_error;
    amxc_string_t current_path;
    amxc_string_t initial_path;
    amxc_llist_t dir_levels;

    amxc_string_init(&current_path, 0);
    amxc_string_init(&initial_path, 0);
    amxc_llist_init(&dir_levels);

    when_str_empty(path, exit);
    amxc_string_set(&initial_path, path);

    when_failed((AMXC_STRING_SPLIT_OK != amxc_string_split_to_llist(&initial_path, &dir_levels, '/')), exit);

    amxc_llist_for_each(iter, &dir_levels) {
        amxc_string_t* current_level = amxc_container_of(iter, amxc_string_t, it);
        const char* c_path = NULL;
        when_null(current_level, exit);
        amxc_string_appendf(&current_path, "/%s", amxc_string_get(current_level, 0));
        c_path = amxc_string_get(&current_path, 0);
        if(!directory_exist(c_path)) {
            SAH_TRACEZ_INFO(ME, "Directory %s not exist create it", c_path);
            when_failed(mkdir(c_path, 0775), exit);
        }
    }

    rc = amxd_status_ok;
exit:
    amxc_string_clean(&current_path);
    amxc_string_clean(&initial_path);
    amxc_llist_clean(&dir_levels, amxc_string_list_it_free);

    return rc;
}

#if defined(CONFIG_SAH_AMX_PCM_TARBALL) && CONFIG_SAH_AMX_PCM_TARBALL
static void remove_files(const char* path, amxc_llist_t* filelist) {
    amxc_string_t filepath;
    amxc_string_init(&filepath, 0);

    when_str_empty(path, exit);
    when_null(filelist, exit);

    amxc_llist_for_each(iter, filelist) {
        amxc_string_t* file = amxc_container_of(iter, amxc_string_t, it);
        amxc_string_setf(&filepath, "%s/%s", path, amxc_string_get(file, 0));
        const char* filepath_str = amxc_string_get(&filepath, 0);

        SAH_TRACEZ_INFO(ME, "remove file %s", filepath_str);

        if(remove(filepath_str) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to delete %s", filepath_str);
        }
    }

exit:
    amxc_string_clean(&filepath);
    return;
}
#endif

bool pcm_tar_create(const char* path, const char* filename) {
    (void) path;
    (void) filename;
    bool ret = true;

#if defined(CONFIG_SAH_AMX_PCM_TARBALL) && CONFIG_SAH_AMX_PCM_TARBALL
    int retval = ARCHIVE_FAILED;
    amxc_string_t tarfile;
    amxc_string_t filepath;
    struct archive* a = NULL;
    struct archive_entry* entry = NULL;
    struct stat st;
    char buff[8192];
    ssize_t len = 0;
    ssize_t len_data = 0;
    int tarfile_fd = -1;
    int file_fd = -1;
    amxc_llist_t* filelist = NULL;

    ret = false;

    amxc_string_init(&filepath, 0);
    amxc_string_init(&tarfile, 0);

    when_str_empty_trace(path, exit, ERROR, "Empty path");

    SAH_TRACEZ_INFO(ME, "Create tarball of the files in directory '%s' ", path);

    filelist = files_from_path(path);
    when_null_trace(filelist, exit, ERROR, "Cannot create file list");

    when_str_empty_trace(filename, exit, ERROR, "Empty file name");

    amxc_string_setf(&tarfile, "%s/%s", path, filename);

    a = archive_write_new();
    when_null_trace(a, exit, ERROR, "Cannot allocate archive object");

    entry = archive_entry_new();
    when_null_trace(entry, exit, ERROR, "Cannot create archive entry");

    retval = archive_write_set_format_gnutar(a);
    when_true_trace(retval != ARCHIVE_OK, exit, ERROR,
                    "Cannot set 'GNU tar' output format");

    tarfile_fd = open(amxc_string_get(&tarfile, 0), O_CREAT | O_WRONLY | O_TRUNC | O_CLOEXEC, 0644);
    when_true_trace(tarfile_fd < 0, exit, ERROR,
                    "Cannot create file %s: %s",
                    amxc_string_get(&tarfile, 0), strerror(errno));

    SAH_TRACEZ_INFO(ME, "Tarball file %s opened for tarbal generation", amxc_string_get(&tarfile, 0));

    retval = archive_write_open_fd(a, tarfile_fd);
    when_true_trace(retval != ARCHIVE_OK, exit, ERROR,
                    "Cannot open archive %s: %s [%d]",
                    amxc_string_get(&tarfile, 0),
                    archive_err_str(a), archive_errno(a));

    amxc_llist_for_each(iter, filelist) {
        amxc_string_t* file = amxc_container_of(iter, amxc_string_t, it);
        amxc_string_setf(&filepath, "%s/%s", path, amxc_string_get(file, 0));
        const char* filename_str = amxc_string_get(file, 0);
        const char* filepath_str = amxc_string_get(&filepath, 0);

        SAH_TRACEZ_INFO(ME, "Add file to tarball %s", filepath_str);

        when_failed_trace(stat(filepath_str, &st), exit_close, ERROR,
                          "Cannot get file %s stats: %s",
                          filepath_str, strerror(errno));

        archive_entry_set_pathname(entry, filename_str);
        archive_entry_copy_stat(entry, &st);

        retval = archive_write_header(a, entry);
        when_true_trace(retval != ARCHIVE_OK, exit_close, ERROR,
                        "Cannot write archive header: %s [%d]",
                        archive_err_str(a), archive_errno(a));

        archive_entry_clear(entry);

        file_fd = open(filepath_str, O_RDONLY);
        when_true_trace(file_fd < 0, exit_close, ERROR,
                        "Cannot open file %s: %s",
                        filepath_str, strerror(errno));

        len = read(file_fd, buff, sizeof(buff));
        while(len > 0) {
            len_data = archive_write_data(a, buff, len);
            SAH_TRACEZ_INFO(ME, "File %s read/write %zd/%zd bytes", filepath_str, len, len_data);
            len = read(file_fd, buff, sizeof(buff));
        }

        close(file_fd);
        file_fd = -1;
    }

    ret = true;

exit_close:
    retval = archive_write_close(a);
    if(retval != ARCHIVE_OK) {
        SAH_TRACEZ_ERROR(ME, "Cannot close archive object: %s [%d]",
                         archive_err_str(a), archive_errno(a));
        ret = false;
    }

exit:
    if(entry != NULL) {
        archive_entry_free(entry);
    }
    if(a != NULL) {
        retval = archive_write_free(a);
        if(retval != ARCHIVE_OK) {
            SAH_TRACEZ_ERROR(ME, "Cannot free archive object: [%d]", retval);
            ret = false;
        }
    }
    if(tarfile_fd != -1) {
        close(tarfile_fd);
    }
    if(filelist != NULL) {
        /* remove files in any case */
        remove_files(path, filelist);
        amxc_llist_delete(&filelist, amxc_string_list_it_free);
    }
    if(ret == true) {
        SAH_TRACEZ_INFO(ME, "Tarball creation success");
    } else {
        SAH_TRACEZ_ERROR(ME, "Tarball creation failure");
        /* remove tarball file on error */
        if(amxc_string_is_empty(&tarfile) == false) {
            remove(amxc_string_get(&tarfile, 0));
        }
    }
    amxc_string_clean(&tarfile);
    amxc_string_clean(&filepath);
#endif
    return ret;
}

bool pcm_tar_extract(const char* path, const char* filename) {
    (void) path;
    (void) filename;
    bool ret = true;

#if defined(CONFIG_SAH_AMX_PCM_TARBALL) && CONFIG_SAH_AMX_PCM_TARBALL
    int retval = ARCHIVE_FAILED;
    amxc_string_t tarfile;
    amxc_string_t filepath;
    const char* tarfile_str = NULL;
    const char* filepath_str = NULL;
    struct archive* a = NULL;
    struct archive_entry* entry = NULL;
    int tarfile_fd = -1;
    int file_fd = -1;

    ret = false;

    amxc_string_init(&tarfile, 0);
    amxc_string_init(&filepath, 0);

    when_str_empty_trace(path, exit, ERROR, "Empty path");
    when_str_empty_trace(filename, exit, ERROR, "Empty file name");

    SAH_TRACEZ_INFO(ME, "Extract files from tarball %s/%s", path, filename);

    amxc_string_setf(&tarfile, "%s/%s", path, filename);
    tarfile_str = amxc_string_get(&tarfile, 0);

    when_false_trace(directory_exist(path), exit, ERROR, "Directory %s does not exist", path);
    when_false_trace(file_exist(tarfile_str), exit, ERROR, "File %s does not exist", tarfile_str);

    tarfile_fd = open(tarfile_str, O_RDONLY);
    when_true_trace(tarfile_fd < 0, exit, ERROR,
                    "Cannot open file %s: %s",
                    tarfile_str, strerror(errno));

    a = archive_read_new();
    when_null_trace(a, exit, ERROR, "Cannot allocate archive object");

    entry = archive_entry_new();
    when_null_trace(entry, exit, ERROR, "Cannot create archive entry");

    retval = archive_read_support_format_gnutar(a);
    when_true_trace(retval != ARCHIVE_OK, exit, ERROR,
                    "Cannot add 'GNU tar' format support: %s [%d]",
                    archive_err_str(a), archive_errno(a));

    retval = archive_read_open_fd(a, tarfile_fd, (1024 * 10));
    when_true_trace(retval != ARCHIVE_OK, exit, ERROR,
                    "Cannot open archive %s: %s [%d]",
                    tarfile_str, archive_err_str(a), archive_errno(a));

    while(true) {
        retval = archive_read_next_header2(a, entry);
        if(retval == ARCHIVE_EOF) {
            SAH_TRACEZ_INFO(ME, "Extracting %s done", tarfile_str);
            break;
        }
        when_true_trace(retval != ARCHIVE_OK, exit_close, ERROR,
                        "Cannot read next header %s: %s [%d]",
                        tarfile_str, archive_err_str(a), archive_errno(a));

        SAH_TRACEZ_INFO(ME, "Extracting file %s", archive_entry_pathname(entry));

        amxc_string_setf(&filepath, "%s/%s", path, archive_entry_pathname(entry));
        filepath_str = amxc_string_get(&filepath, 0);

        file_fd = open(filepath_str, O_CREAT | O_WRONLY | O_TRUNC | O_CLOEXEC, 0644);
        when_true_trace(file_fd < 0, exit_close, ERROR,
                        "Cannot create file %s: %s",
                        filepath_str, strerror(errno));

        retval = archive_read_data_into_fd(a, file_fd);
        when_true_trace(retval != ARCHIVE_OK, exit_close, ERROR,
                        "Cannot write data to file %s: %s [%d]",
                        filepath_str, archive_err_str(a), archive_errno(a));

        close(file_fd);
        file_fd = -1;
    }

    ret = true;

exit_close:
    retval = archive_read_close(a);
    if(retval != ARCHIVE_OK) {
        SAH_TRACEZ_WARNING(ME, "Cannot close archive object: %s [%d]",
                           archive_err_str(a), archive_errno(a));
    }

exit:
    if(entry != NULL) {
        archive_entry_free(entry);
    }
    if(file_fd != -1) {
        close(file_fd);
    }
    if(tarfile_fd != -1) {
        close(tarfile_fd);
    }
    if(a != NULL) {
        retval = archive_read_free(a);
        if(retval != ARCHIVE_OK) {
            SAH_TRACEZ_WARNING(ME, "Cannot free archive object: [%d]", retval);
        }
    }
    if(ret == true) {
        SAH_TRACEZ_INFO(ME, "Tarball extraction success");
        /* remove tarball file on success */
        remove(amxc_string_get(&tarfile, 0));
    } else {
        SAH_TRACEZ_ERROR(ME, "Tarball extraction failure");
    }
    amxc_string_clean(&tarfile);
    amxc_string_clean(&filepath);
#endif
    return ret;
}

#undef ME
