/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <stdio.h>
#include <string.h>

#include "dm_pcm_service.h"
#include "dm_pcm.h"
#include "dm_pcm_mngr.h"
#include "fs_utils.h"
#include "dm_pcm_crypt.h"

#define PCMTAR_NAME "user_backup.tar"
#define ME "pcm"

static amxd_status_t pcm_store_backup(const char* service, amxc_var_t* to_store, const char* type);
static const char* pcm_mod_names[pcm_mod_max] = { "pcm-ctrl-json", "pcm-ctrl-xml"};

amxc_string_t* date_prefix_to_file(const char* filename, amxc_ts_t* date) {
    char* date_prefix = NULL;
    amxc_string_t* date_filename = NULL;

    amxc_string_new(&date_filename, 0);

    when_str_empty_trace(filename, exit, ERROR, "Filename is not provided");
    when_null_trace(date, exit, ERROR, "Date is not provided");

    date_prefix = (char*) calloc(37, sizeof(char));
    when_null_trace(date_prefix, exit, ERROR, "Could not allocate menory");

    amxc_ts_format(date, date_prefix, 36);
    amxc_string_setf(date_filename, "%s_%s", date_prefix, filename);

exit:
    free(date_prefix);
    return date_filename;
}

int pcm_backup_service(amxd_object_t* service, const char* type, const char* userflags) {
    int retval = 1;
    char* service_object = amxd_object_get_value(cstring_t, service, "Name", NULL);
    char* name = amxd_object_get_value(cstring_t, service, "Alias", NULL);
    char* export = amxd_object_get_value(cstring_t, service, "ExportCallback", NULL);
    char* service_status = amxd_object_get_value(cstring_t, service, "ServiceStatus", NULL);
    amxc_var_t ret;
    amxc_var_t* backup_data = NULL;
    amxb_bus_ctx_t* ctx = NULL;
    uint32_t current_access = 0;
    amxc_var_t export_args;

    amxc_var_init(&ret);
    amxc_var_init(&export_args);
    amxc_var_set_type(&export_args, AMXC_VAR_ID_HTABLE);

    when_str_empty_trace(name, exit, ERROR, "Alias of the service is empty");
    when_str_empty_trace(service_object, exit, ERROR, "[%s] Service Object is empty", name);
    when_str_empty_trace(export, exit, ERROR, "[%s] ExportCallback function is not provided", name);
    when_str_empty_trace(service_status, exit, ERROR, "[%s] ServiceStatus is empty", name);

    //Just skip the Service if it is Offline.
    when_true_status(strcmp(service_status, "Offline") == 0, exit, retval = 0);
    SAH_TRACEZ_INFO(ME, "Backup service %s with export function %s", name, export);

    ctx = amxb_be_who_has(service_object);
    when_null_trace(ctx, exit, ERROR, "Could not find bus for the [%s]", service_object);

    amxc_var_add_key(cstring_t, &export_args, "Flags", userflags);
    amxc_var_add_key(bool, &export_args, "changed", true);
    amxc_var_add_key(bool, &export_args, "usersetting", (strcmp(type, "export") == 0)? true:false);

    current_access = ctx->access;
    amxb_set_access(ctx, AMXB_PROTECTED);
    retval = amxb_call(ctx, service_object, export, &export_args, &ret, 5);
    amxb_set_access(ctx, current_access);
    if(AMXB_STATUS_OK != retval) {
        SAH_TRACEZ_ERROR(ME, "Export [%s] function failed on service %s %s [%d]", export, name, amxd_status_string(retval), retval);
        pcm_mark_service_action(name, "ExportStatus", "Error");
        goto exit;
    }

    backup_data = amxc_var_get_path(&ret, "1.args", AMXC_VAR_FLAG_DEFAULT);
    if(backup_data == NULL) {
        backup_data = amxc_var_get_path(&ret, "1.", AMXC_VAR_FLAG_DEFAULT);
    }

    if(AMXB_STATUS_OK != pcm_store_backup(name, backup_data, type)) {
        SAH_TRACEZ_ERROR(ME, "Store backup file for %s failed", name);
        pcm_mark_service_action(name, "ExportStatus", "Error");
        goto exit;
    }

    pcm_mark_service_action(name, "ExportStatus", "Success");
    retval = 0;

exit:
    free(service_object);
    free(export);
    free(name);
    free(service_status);
    amxc_var_clean(&ret);
    amxc_var_clean(&export_args);
    return retval;
}

int pcm_restore_service(amxd_object_t* service, const char* type) {
    int retval = 1;
    char* service_object = amxd_object_get_value(cstring_t, service, "Name", NULL);
    char* import = amxd_object_get_value(cstring_t, service, "ImportCallback", NULL);
    char* name = amxd_object_get_value(cstring_t, service, "Alias", NULL);
    char* service_status = amxd_object_get_value(cstring_t, service, "ServiceStatus", NULL);
    amxc_var_t backup;
    amxc_var_t ret;
    amxb_bus_ctx_t* ctx = NULL;
    uint32_t current_access = 0;
    amxc_string_t* file_name = NULL;

    amxc_var_init(&backup);
    amxc_var_init(&ret);
    amxc_var_set_type(&backup, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&ret, AMXC_VAR_ID_HTABLE);

    when_str_empty_trace(name, exit, ERROR, "Alias of the service is empty");
    when_str_empty_trace(service_object, exit, ERROR, "[%s] Service Object is empty", name);
    when_str_empty_trace(import, exit, ERROR, "[%s] ExportCallback function is not provided", name);
    when_str_empty_trace(service_status, exit, ERROR, "[%s] ServiceStatus is empty", name);

    //Just skip the Service if it is Offline.
    when_true_status(strcmp(service_status, "Offline") == 0, exit, retval = 0);
    SAH_TRACEZ_INFO(ME, "Restore service %s with import function %s", name, import);

    ctx = amxb_be_who_has(service_object);
    when_null_trace(ctx, exit, ERROR, "Could not find bus for the [%s]", service_object);

    if(AMXB_STATUS_OK != pcm_load_backup(name, &backup, type)) {
        SAH_TRACEZ_ERROR(ME, "Load backup file for %s failed", name);
        pcm_mark_service_action(name, "ImportStatus", "Error");
        goto exit;
    }

    current_access = ctx->access;
    amxb_set_access(ctx, AMXB_PROTECTED);
    retval = amxb_call(ctx, service_object, import, &backup, &ret, 5);
    amxb_set_access(ctx, current_access);
    if(AMXB_STATUS_OK != retval) {
        SAH_TRACEZ_ERROR(ME, "Import [%s] function failed on service %s %s [%d]", import, name, amxd_status_string(retval), retval);
        pcm_mark_service_action(name, "ImportStatus", "Error");
        goto exit;
    }
    file_name = pcm_get_backup_file(name);
    remove_file(pcm_get_store_path(type), amxc_string_get(file_name, 0));

    pcm_mark_service_action(name, "ImportStatus", "Success");
    retval = amxd_status_ok;

exit:
    free(service_object);
    free(import);
    free(name);
    free(service_status);
    amxc_var_clean(&backup);
    amxc_var_clean(&ret);
    amxc_string_delete(&file_name);
    return retval;
}

amxd_status_t pcm_mark_service_action(const char* name, const char* action, const char* state) {
    amxd_status_t rc = amxd_status_unknown_error;
    amxd_trans_t transaction;

    amxd_trans_init(&transaction);

    when_str_empty(name, exit);
    when_str_empty(action, exit);
    when_str_empty(state, exit);

    SAH_TRACEZ_INFO(ME, "Changing Service.%s.%s to %s", name, action, state);

    amxd_trans_select_pathf(&transaction, "%s.Service.%s", pcm_root_object(), name);
    amxd_trans_set_value(cstring_t, &transaction, action, state);
    rc = amxd_trans_apply(&transaction, pcm_get_dm());

exit:
    amxd_trans_clean(&transaction);
    return rc;
}

amxd_status_t pcm_load_backup(const char* filename, amxc_var_t* const backup, const char* type) {
    amxd_status_t rc = amxd_status_unknown_error;
    const char* pcm_ctrl = pcm_get_ctrl();
    amxc_var_t data;
    const char* file_path = NULL;

    amxc_var_init(&data);

    when_str_empty_trace(filename, exit, ERROR, "File name is not provided");
    when_str_empty_trace(pcm_ctrl, exit, ERROR, "Could not find controller");
    when_null_trace(backup, exit, ERROR, "backup argument isn't initialized");

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    pcm_backup_file_path(filename, &data, type);
    file_path = GET_CHAR(&data, "name");
    rc = amxd_status_file_not_found;
    when_false_trace(file_exist(file_path), exit, WARNING, "File [%s] does not exist", file_path);

    if(0 != amxm_execute_function(pcm_ctrl, pcm_mod_names[pcm_get_file_type()], "load-backup", &data, backup)) {
        SAH_TRACEZ_ERROR(ME, "Cannot execute ctrl %s function load-backup", pcm_ctrl);
        goto exit;
    }
    rc = amxd_status_ok;

exit:
    amxc_var_clean(&data);
    return rc;
}

bool prepare_exported_backup(amxc_var_t* results, const char* tag) {
    bool ret_val = false;
    amxd_object_t* security = NULL;
    amxd_object_t* root_obj = NULL;
    amxc_ts_t now;
    amxc_string_t* filename = NULL;
    const char* path = NULL;

    amxc_ts_now(&now);

    root_obj = amxd_dm_findf(pcm_get_dm(), "%s", pcm_root_object());
    when_null_trace(root_obj, exit, ERROR, "Could not find object %s", pcm_root_object());
    security = amxd_dm_findf(pcm_get_dm(), "%s.Config.Security.", pcm_root_object());
    when_null_trace(security, exit, ERROR, "Could not find object %s.Config.Security.", pcm_root_object());

    path = pcm_get_store_path("export");
    if(amxd_object_get_value(bool, security, "EncryptUserFile", NULL)) {
        char* base64_key = NULL;
        base64_key = amxd_object_get_value(cstring_t, security, "EncryptKey", NULL);
        pcm_encrypt_dir(path, base64_key);
        free(base64_key);
    }

    filename = date_prefix_to_file(PCMTAR_NAME, &now);
    when_null_trace(filename, exit, ERROR, "Could not add date prefix to the %s", PCMTAR_NAME);
    ret_val = pcm_tar_create(path, amxc_string_get(filename, 0));
    when_false_trace(ret_val, exit, ERROR, "Could not create tarball file");
    when_failed_trace(pcm_add_backup_file(amxc_string_get(filename, 0), tag, &now, results), exit, ERROR, "Failed to add new BackupFile instance");
    SAH_TRACEZ_INFO(ME, "Prepare of the user setting backup file is done successfully");

exit:
    amxc_string_delete(&filename);
    return ret_val;
}

amxd_status_t pcm_add_backup_file(const char* filename, const char* tag, amxc_ts_t* date, amxc_var_t* results) {
    amxd_trans_t trans;
    amxd_status_t ret = amxd_status_unknown_error;
    char* alias = NULL;

    amxd_trans_init(&trans);

    when_str_empty_trace(filename, exit, ERROR, "Filename is not provided");
    when_str_empty_trace(tag, exit, ERROR, "Tag is not provided");
    when_null_trace(date, exit, ERROR, "Date is not provided");

    ret = amxd_trans_select_pathf(&trans, "%s.BackupFile.", pcm_root_object());
    when_failed_trace(ret, exit, ERROR, "Transaction could not select the %s.BackupFile.", pcm_root_object());

    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(amxc_ts_t, &trans, "CreationDate", date);
    amxd_trans_set_value(cstring_t, &trans, "Tag", tag);
    amxd_trans_set_value(cstring_t, &trans, "FileName", filename);

    ret = amxd_trans_apply(&trans, pcm_get_dm());
    when_failed_trace(ret, exit, ERROR, "Transaction failed with %d", ret);

    alias = amxd_object_get_value(cstring_t, trans.current, "Alias", NULL);
    amxc_var_set_type(results, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, results, "Alias", alias);
    amxc_var_add_key(amxc_ts_t, results, "CreationDate", date);
    amxc_var_add_key(cstring_t, results, "FileName", filename);
    SAH_TRACEZ_INFO(ME, "BackupFile instance [%s] was added successfully", alias);

exit:
    free(alias);
    amxd_trans_clean(&trans);
    return ret;
}

amxd_status_t pcm_del_backup_file(const char* alias) {
    amxd_trans_t trans;
    amxd_status_t ret = amxd_status_unknown_error;

    amxd_trans_init(&trans);

    when_str_empty_trace(alias, exit, ERROR, "Instance alias in not provided");

    ret = amxd_trans_select_pathf(&trans, "%s.BackupFile.", pcm_root_object());
    when_failed_trace(ret, exit, ERROR, "Transaction could not select the %s.BackupFile.", pcm_root_object());

    amxd_trans_del_inst(&trans, 0, alias);
    ret = amxd_trans_apply(&trans, pcm_get_dm());
    when_failed_trace(ret, exit, ERROR, "Transaction failed with %d", ret);
    SAH_TRACEZ_INFO(ME, "BackupFile instance [%s] was deleted successfully", alias);

exit:
    amxd_trans_clean(&trans);
    return ret;
}

bool prepare_exported_restore(const char* alias) {
    bool ret_val = false;
    amxd_object_t* security = NULL;
    amxd_object_t* backup_file = NULL;
    char* file_name = NULL;
    const char* path = NULL;
    const char* root_obj_str = pcm_root_object();

    when_str_empty_trace(alias, exit, ERROR, "Alias is not provided");

    security = amxd_dm_findf(pcm_get_dm(), "%s.Config.Security.", root_obj_str);
    when_null_trace(security, exit, ERROR, "Could not find object %s.Config.Security.", root_obj_str);
    backup_file = amxd_dm_findf(pcm_get_dm(), "%s.BackupFile.%s.", root_obj_str, alias);
    when_null_trace(backup_file, exit, ERROR, "Could not find object %s.BackupFile.%s ", root_obj_str, alias);

    file_name = amxd_object_get_value(cstring_t, backup_file, "FileName", NULL);
    when_str_empty_trace(file_name, exit, ERROR, "Empty FileName of %s.BackupFile.%s ", root_obj_str, alias);


    path = pcm_get_store_path("export");
    ret_val = pcm_tar_extract(path, file_name);
    when_false_trace(ret_val, exit, ERROR, "Could not extract the tarball file %s", file_name);
    pcm_del_backup_file(alias);

    if(amxd_object_get_value(bool, security, "EncryptUserFile", NULL)) {
        char* base64_key = NULL;
        base64_key = amxd_object_get_value(cstring_t, security, "EncryptKey", NULL);
        pcm_decrypt_dir(path, base64_key);
        free(base64_key);
    }

exit:
    free(file_name);
    return ret_val;
}

static amxd_status_t pcm_store_backup(const char* service, amxc_var_t* to_store, const char* type) {
    amxd_status_t rc = amxd_status_unknown_error;
    const char* pcm_ctrl = pcm_get_ctrl();
    amxc_var_t store_args;
    amxc_var_t ret;
    amxc_var_t* content_to_store = NULL;

    amxc_var_init(&store_args);
    amxc_var_init(&ret);

    when_str_empty_trace(service, exit, ERROR, "Service name is not provided");
    when_str_empty_trace(pcm_ctrl, exit, ERROR, "Could not find controller");
    when_null_trace(to_store, exit, ERROR, "No data to store");

    content_to_store = amxc_var_get_first(to_store);
    when_null_trace(content_to_store, exit, ERROR, "Content of backup data is empty");

    amxc_var_set_type(&store_args, AMXC_VAR_ID_HTABLE);
    pcm_backup_file_path(service, &store_args, type);
    amxc_var_add_key(bool, &store_args, "beautify", true);
    amxc_var_add_key(amxc_htable_t, &store_args, "content", amxc_var_get_const_amxc_htable_t(content_to_store));

    if(0 != amxm_execute_function(pcm_ctrl, pcm_mod_names[pcm_get_file_type()], "store-backup", &store_args, &ret)) {
        SAH_TRACEZ_ERROR(ME, "Cannot execute ctrl %s function store-backup", pcm_ctrl);
        goto exit;
    }
    rc = amxd_status_ok;

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&store_args);
    return rc;
}

#undef ME
