include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C odl all
	$(MAKE) -C mod-pcm-json/src all
	$(MAKE) -C mod-pcm-xml/src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C odl clean
	$(MAKE) -C test clean
	$(MAKE) -C mod-pcm-json/src clean
	$(MAKE) -C mod-pcm-xml/src clean

install: all
	$(INSTALL) -D -p -m 0644 odl/pcm-manager.odl $(DEST)/etc/amx/$(COMPONENT)/pcm-manager.odl
	$(INSTALL) -D -p -m 0644 odl/pcm-manager_caps.odl $(DEST)/etc/amx/$(COMPONENT)/pcm-manager_caps.odl
	$(INSTALL) -D -p -m 0644 odl/PersistentConfiguration-definition.odl $(DEST)/etc/amx/$(COMPONENT)/PersistentConfiguration-definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-pcm_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/PersistentConfiguration-service.odl $(DEST)/etc/amx/$(COMPONENT)/PersistentConfiguration-service.odl
	$(INSTALL) -D -p -m 0644 odl/PersistentConfiguration-schema.odl $(DEST)/etc/amx/$(COMPONENT)/PersistentConfiguration-schema.odl
	$(INSTALL) -D -p -m 0644 odl/PersistentConfiguration-config.odl $(DEST)/etc/amx/$(COMPONENT)/PersistentConfiguration-config.odl
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(DEST)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/object/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-pcm-json.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-pcm-json.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-pcm-xml.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-pcm-xml.so
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/tr181-pcm.sh $(DEST)$(INITDIR)/$(COMPONENT)

package: all
	$(INSTALL) -D -p -m 0644 odl/pcm-manager.odl $(PKGDIR)/etc/amx/$(COMPONENT)/pcm-manager.odl
	$(INSTALL) -D -p -m 0644 odl/pcm-manager_caps.odl $(PKGDIR)/etc/amx/$(COMPONENT)/pcm-manager_caps.odl
	$(INSTALL) -D -p -m 0644 odl/PersistentConfiguration-definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/PersistentConfiguration-definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-pcm_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/PersistentConfiguration-service.odl $(PKGDIR)/etc/amx/$(COMPONENT)/PersistentConfiguration-service.odl
	$(INSTALL) -D -p -m 0644 odl/PersistentConfiguration-schema.odl $(PKGDIR)/etc/amx/$(COMPONENT)/PersistentConfiguration-schema.odl
	$(INSTALL) -D -p -m 0644 odl/PersistentConfiguration-config.odl $(PKGDIR)/etc/amx/$(COMPONENT)/PersistentConfiguration-config.odl
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/object/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-pcm-json.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-pcm-json.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-pcm-xml.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-pcm-xml.so
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/tr181-pcm.sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(eval ODLFILES += odl/pcm-manager.odl)
	$(eval ODLFILES += odl/pcm-manager_caps.odl)
	$(eval ODLFILES += odl/PersistentConfiguration-definition.odl)
	$(eval ODLFILES += odl/$(COMPONENT)_mapping.odl)
	$(eval ODLFILES += odl/PersistentConfiguration-service.odl)
	$(eval ODLFILES += odl/PersistentConfiguration-schema.odl)
	$(eval ODLFILES += odl/PersistentConfiguration-config.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C mod-pcm-json/test run
	$(MAKE) -C mod-pcm-xml/test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test