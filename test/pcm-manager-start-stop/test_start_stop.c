/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#include "../common/test_helper.h"
#include "dm_pcm.h"
#include "dm_pcm_rpc.h"

#include "test_start_stop.h"
#define  PCM_ROOT_OBJ "PersistentConfiguration"

#define TEST_ROOT_DIR "tmp/"
#define CREATE_TEST_DIR "mkdir -p "TEST_ROOT_DIR
#define REMOVE_TEST_DIR "rm -rf "TEST_ROOT_DIR


static pcm_test_helper test_env;
static amxd_dm_t* test_pcm_get_dm(void);

int test_setup(UNUSED void** state) {
    assert_int_equal(0, system(CREATE_TEST_DIR));
    pcm_test_helper_setup(&test_env);
    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_int_equal(0, system(REMOVE_TEST_DIR));
    pcm_clean_ctx();
    pcm_test_helper_teardown(&test_env);
    return 0;
}

void test_dm_is_set(UNUSED void** state) {
    assert_ptr_equal(pcm_get_dm(), test_env.dm);
}

void test_can_register(UNUSED void** state) {
    const char* const service_name = "PcmManagerTest";
    const char* const name = "pcm-test";
    amxd_object_t* service_obj = NULL;
    amxc_var_t* data = NULL;
    amxc_var_t* info = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t command;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&command, 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", name);
    data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);
    info = amxc_var_add_key(amxc_htable_t, data, "info", NULL);
    amxc_var_add_key(cstring_t, info, "rpcImport", "ImportTest");
    amxc_var_add_key(cstring_t, info, "rpcExport", "ExportTest");
    amxc_var_add_key(int32_t, info, "priority", 0);
    amxc_var_add_key(cstring_t, info, "dmRoot", service_name);
    amxc_var_add_key(cstring_t, info, "version", "1.0");
    amxc_var_add_key(cstring_t, info, "name", name);
    amxc_var_add_key(bool, info, "outOfOrder", true);

    //Create a dummy backup file
    amxc_string_setf(&command, "touch %s%s.json", TEST_ROOT_DIR, name);
    assert_int_equal(0, system(amxc_string_get(&command, 0)));

    expect_function_call(__wrap_amxm_execute_function);
    assert_int_equal(_registerSvc(NULL, NULL, &args, &ret), amxd_status_ok);
    pcm_wait_for_timer_event();
    service_obj = amxd_object_findf(amxd_dm_get_root(test_pcm_get_dm()),
                                    "%s.Service.%s",
                                    PCM_ROOT_OBJ, name);
    assert_non_null(service_obj);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&command);
}

void test_can_unregister(UNUSED void** state) {
    const char* const service_name = "PcmManagerTest";
    const char* const name = "pcm-test";
    amxd_object_t* service_obj = NULL;
    char* status = NULL;
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&args, "name", name);

    service_obj = amxd_object_findf(amxd_dm_get_root(test_pcm_get_dm()),
                                    "%s.Service.%s",
                                    PCM_ROOT_OBJ, name);
    assert_non_null(service_obj);
    assert_int_equal(_unregisterSvc(NULL, NULL, &args, NULL), amxd_status_ok);

    service_obj = amxd_object_findf(amxd_dm_get_root(test_pcm_get_dm()),
                                    "%s.Service.%s",
                                    PCM_ROOT_OBJ, name);
    assert_non_null(service_obj);
    pcm_handle_events();
    status = amxd_object_get_cstring_t(service_obj, "ServiceStatus", NULL);
    assert_string_equal(status, "Offline");
    amxc_var_clean(&args);
    free(status);
}

void test_can_restore(UNUSED void** state) {
    amxc_string_t command;
    const char* const name = "pcm-test";
    amxd_object_t* service_obj = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_string_init(&command, 0);
    amxc_string_setf(&command, "touch %s%s.json", TEST_ROOT_DIR, name);
    assert_int_equal(0, system(amxc_string_get(&command, 0)));

    service_obj = amxd_object_findf(amxd_dm_get_root(test_pcm_get_dm()),
                                    "%s.Service.%s",
                                    PCM_ROOT_OBJ, name);
    assert_non_null(service_obj);
    expect_function_call(__wrap_amxm_execute_function);
    assert_int_equal(_Restore(NULL, NULL, &args, &ret), amxd_status_ok);
    amxc_string_clean(&command);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_can_restore_usersetting(UNUSED void** state) {
    amxc_var_t args;
    amxd_object_t* backup_file_obj = NULL;
    char* inst_alias = NULL;
    char* inst_tag = NULL;

    backup_file_obj = amxd_object_findf(amxd_dm_get_root(test_pcm_get_dm()),
                                        "%s.BackupFile.",
                                        PCM_ROOT_OBJ);
    assert_non_null(backup_file_obj);

    amxd_object_for_each(instance, it, backup_file_obj) {
        amxd_object_t* inst = amxc_container_of(it, amxd_object_t, it);
        inst_alias = amxd_object_get_value(cstring_t, inst, "Alias", NULL);
        inst_tag = amxd_object_get_value(cstring_t, inst, "Tag", NULL);
        if(strcmp(inst_tag, "Manual") == 0) {
            break;
        }
        free(inst_alias);
        free(inst_tag);
    }

    assert_non_null(inst_alias);
    assert_non_null(inst_tag);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&args, "Type", "export");
    amxc_var_add_new_key_cstring_t(&args, "FileRef", inst_alias);

    free(inst_alias);
    free(inst_tag);

    set_func_return_value(success);
    expect_function_call(__wrap_amxm_execute_function);
    set_tar_extract_to_fail(false);
    set_func_return_value(called);
    assert_int_equal(_Restore(NULL, NULL, &args, NULL), amxd_status_ok);

    amxc_var_clean(&args);

}

void test_service_backup_when_notified(UNUSED void** state) {
    const char* const name = "pcm-test";
    amxd_object_t* service_obj = NULL;
    amxd_object_t* pcm_obj = NULL;
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&args, "name", name);

    service_obj = amxd_object_findf(amxd_dm_get_root(test_pcm_get_dm()),
                                    "%s.Service.%s",
                                    PCM_ROOT_OBJ, name);

    pcm_obj = amxd_object_findf(amxd_dm_get_root(test_pcm_get_dm()),
                                "%s.Config",
                                PCM_ROOT_OBJ);
    assert_non_null(service_obj);
    assert_non_null(pcm_obj);

    assert_int_equal(_ServiceConfigurationChanged(NULL, NULL, &args, NULL), amxd_status_ok);
    pcm_wait_for_timer_event();

    assert_int_equal(amxd_object_set_cstring_t(pcm_obj, "AutoSync", "ForceEnable"), amxd_status_ok);
    expect_function_call(__wrap_amxm_execute_function);
    assert_int_equal(_ServiceConfigurationChanged(NULL, NULL, &args, NULL), amxd_status_ok);
    pcm_wait_for_timer_event();

    assert_int_equal(amxd_object_set_cstring_t(pcm_obj, "AutoSync", "Enable"), amxd_status_ok);
    expect_function_call(__wrap_amxm_execute_function);
    assert_int_equal(_ServiceConfigurationChanged(NULL, NULL, &args, NULL), amxd_status_ok);
    pcm_wait_for_timer_event();

    amxc_var_clean(&args);
}

void test_check_import(UNUSED void** state) {
    amxd_object_t* pcm_security = amxd_object_findf(amxd_dm_get_root(test_pcm_get_dm()),
                                                    "%s.Config.Security.",
                                                    PCM_ROOT_OBJ);
    bool encrypt_user_files = false;
    amxc_var_t args;
    amxc_var_t* content = NULL;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    assert_non_null(pcm_security);
    encrypt_user_files = amxd_object_get_bool(pcm_security, "EncryptUserFile", NULL);
    assert_false(encrypt_user_files);

    content = amxc_var_add_key(amxc_htable_t, &args, "content", NULL);
    content = amxc_var_add_key(amxc_htable_t, content, "upc", NULL);
    content = amxc_var_add_key(amxc_htable_t, content, "PersistentConfiguration.Config.Security.", NULL);
    amxc_var_add_key(bool, content, "EncryptUserFile", true);

    assert_int_equal(_Import(NULL, NULL, &args, NULL), amxd_status_ok);
    encrypt_user_files = amxd_object_get_bool(pcm_security, "EncryptUserFile", NULL);
    assert_true(encrypt_user_files);

    amxc_var_clean(&args);
}

void test_check_Backup(UNUSED void** state) {
    const char* const name = "pcm-test";
    amxd_object_t* service_obj = NULL;
    amxd_object_t* pcm_obj = NULL;
    char* status = NULL;
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "usersetting", false);

    service_obj = amxd_object_findf(amxd_dm_get_root(test_pcm_get_dm()),
                                    "%s.Service.%s",
                                    PCM_ROOT_OBJ, name);

    expect_function_call(__wrap_amxm_execute_function);
    assert_int_equal(_Backup(NULL, NULL, &args, NULL), amxd_status_ok);
    pcm_wait_for_timer_event();

    status = amxd_object_get_cstring_t(service_obj, "ExportStatus", NULL);
    assert_string_equal(status, "Success");

    free(status);
    amxc_var_clean(&args);
}

void test_check_backup_usersetting(UNUSED void** state) {
    const char* const name = "pcm-test";
    uint32_t max_backup_number = 1;
    uint32_t number_of_entries = 0;
    amxd_object_t* service_obj = NULL;
    amxd_object_t* backup_file_obj = NULL;
    amxd_object_t* pcm_obj = NULL;
    char* status = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t command;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_string_init(&command, 0);
    amxc_string_setf(&command, "touch %s%s.json", TEST_ROOT_DIR, name);
    assert_int_equal(0, system(amxc_string_get(&command, 0)));

    amxc_var_add_key(cstring_t, &args, "Type", "export");
    amxc_var_add_key(cstring_t, &args, "Tag", "Manual");

    service_obj = amxd_object_findf(amxd_dm_get_root(test_pcm_get_dm()),
                                    "%s.Service.%s",
                                    PCM_ROOT_OBJ, name);

    expect_function_call(__wrap_amxm_execute_function);
    set_tar_append_to_fail(false);
    set_func_return_value(called);
    assert_int_equal(_Backup(NULL, NULL, &args, &ret), amxd_status_ok);
    pcm_wait_for_timer_event();

    status = amxd_object_get_value(cstring_t, service_obj, "ExportStatus", NULL);
    assert_string_equal(status, "Success");

    backup_file_obj = amxd_object_findf(amxd_dm_get_root(test_pcm_get_dm()),
                                        "%s.BackupFile.",
                                        PCM_ROOT_OBJ);


    assert_non_null(backup_file_obj);
    amxd_object_for_each(instance, it, backup_file_obj) {
        amxd_object_t* inst = amxc_container_of(it, amxd_object_t, it);
        char* inst_tag = amxd_object_get_value(cstring_t, inst, "Tag", NULL);
        if(strcmp(inst_tag, "Manual") == 0) {
            number_of_entries++;
        }
        free(inst_tag);
    }

    assert_int_equal(number_of_entries, max_backup_number);

    free(status);
    amxc_string_clean(&command);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_support_service_restart(UNUSED void** state) {
    const char* const service_name = "PcmManagerTest";
    const char* const name = "pcm-test";
    amxd_object_t* service_obj = NULL;
    amxc_var_t args;
    char* status = NULL;
    amxc_var_t* data = NULL;
    amxc_var_t* info = NULL;

    amxc_var_init(&args);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", name);
    data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);
    info = amxc_var_add_key(amxc_htable_t, data, "info", NULL);
    amxc_var_add_key(cstring_t, info, "rpcImport", "ImportTest");
    amxc_var_add_key(cstring_t, info, "rpcExport", "ExportTest");
    amxc_var_add_key(int32_t, info, "priority", 0);
    amxc_var_add_key(cstring_t, info, "dmRoot", service_name);
    amxc_var_add_key(cstring_t, info, "version", "1.0");
    amxc_var_add_key(cstring_t, info, "name", name);
    amxc_var_add_key(bool, info, "outOfOrder", true);

    assert_int_equal(_registerSvc(NULL, NULL, &args, NULL), amxd_status_ok);
    service_obj = amxd_object_findf(amxd_dm_get_root(test_pcm_get_dm()),
                                    "%s.Service.%s",
                                    PCM_ROOT_OBJ, name);
    assert_non_null(service_obj);
    status = amxd_object_get_cstring_t(service_obj, "ServiceStatus", NULL);
    assert_string_equal(status, "Online");
    free(status);

    assert_int_equal(_unregisterSvc(NULL, NULL, &args, NULL), amxd_status_ok);
    pcm_handle_events();

    status = amxd_object_get_cstring_t(service_obj, "ServiceStatus", NULL);
    assert_string_equal(status, "Offline");
    free(status);

    assert_int_equal(_registerSvc(NULL, NULL, &args, NULL), amxd_status_ok);
    pcm_handle_events();

    status = amxd_object_get_cstring_t(service_obj, "ServiceStatus", NULL);
    assert_string_equal(status, "Online");
    free(status);

    amxc_var_clean(&args);
}

static amxd_dm_t* test_pcm_get_dm(void) {
    return test_env.dm;
}
