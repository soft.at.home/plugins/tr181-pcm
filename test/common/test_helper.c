/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxc/amxc_variant.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>
#include <cmocka.h>
#include <archive.h>
#include <archive_entry.h>


#include "test_helper.h"
#include "dm_pcm.h"
#include "dm_pcm_rpc.h"
#include "dm_pcm_event.h"
extern int __real_archive_write_header(struct archive*, struct archive_entry*);
extern int __real_archive_read_data_into_fd(struct archive*, int fd);
extern bool __real_pcm_tar_create(const char* path, const char* file_name);
extern bool __real_pcm_tar_extract(const char* path, const char* file_name);
extern amxb_bus_ctx_t* __real_amxb_be_who_has(const char* object_path);

static amxp_signal_t* signal_handler = NULL;
static func_return_t return_function_value = called;
static bool tar_append_to_fail = false;
static bool tar_extract_to_fail = false;
static amxb_bus_ctx_t* ctx;

void pcm_clean_ctx(void) {
    if(ctx != NULL) {
        free(ctx);
    }
}

static amxd_status_t ImportTest(amxd_object_t* object,
                                amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret);

static amxd_status_t ExportTest(amxd_object_t* object,
                                amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret);

void set_func_return_value(func_return_t value) {
    return_function_value = value;
}

void set_tar_append_to_fail(bool value) {
    tar_append_to_fail = value;
}

void set_tar_extract_to_fail(bool value) {
    tar_extract_to_fail = value;
}

int __wrap_amxm_execute_function(UNUSED const char* const shared_object_name,
                                 UNUSED const char* const module_name,
                                 const char* const func_name,
                                 UNUSED amxc_var_t* args,
                                 amxc_var_t* ret) {
    if((NULL != func_name) && (0 == strcmp(func_name, "load-backup"))) {
        amxc_var_t* content = amxc_var_add_key(amxc_htable_t, ret, "content", NULL);
        amxc_var_add_key(cstring_t, content, "Name", "RestoreName");
        function_called();

    } else if((NULL != func_name) && (0 == strcmp(func_name, "store-backup"))) {
        function_called();
    }
    return 0;
}

bool __wrap_pcm_tar_create(const char* path, const char* file_name) {

    switch(return_function_value) {
    case success:
        return true;
    case failed:
        return false;
    default:
        return __real_pcm_tar_create(path, file_name);
    }
}

bool __wrap_pcm_tar_extract(const char* path, const char* file_name) {

    switch(return_function_value) {
    case success:
        return true;
    case failed:
        return false;
    default:
        return __real_pcm_tar_extract(path, file_name);
    }
}

int __wrap_archive_write_header(struct archive* a, struct archive_entry* e) {
    if(tar_append_to_fail) {
        printf("[tar create] Call mock archive_write_header\n");
        tar_append_to_fail = false;
        return ARCHIVE_FATAL;
    } else {
        return __real_archive_write_header(a, e);
    }
}

int __wrap_archive_read_data_into_fd(struct archive* a, int fd) {
    if(tar_extract_to_fail) {
        printf("[tar extract] Call mock archive_read_data\n");
        tar_extract_to_fail = false;
        return ARCHIVE_FATAL;
    } else {
        return __real_archive_read_data_into_fd(a, fd);
    }
}

void pcm_test_helper_setup(pcm_test_helper* helper) {
    amxd_dm_t* dm = NULL;
    amxo_parser_t* parser = NULL;
    amxd_object_t* root_obj = NULL;
    amxb_bus_ctx_t* bus_ctx = NULL;

    assert_int_equal(amxd_dm_new(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_new(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);
    assert_int_equal(amxp_signal_new(NULL, &signal_handler, strsignal(SIGCHLD)), 0);

    root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(parser, "registerSvc", AMXO_FUNC(_registerSvc)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "unregisterSvc", AMXO_FUNC(_unregisterSvc)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ServiceConfigurationChanged", AMXO_FUNC(_ServiceConfigurationChanged)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "check_is_empty_or_in", AMXO_FUNC(_check_is_empty_or_in)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "Import", AMXO_FUNC(_Import)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "Backup", AMXO_FUNC(_Backup)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ImportTest", AMXO_FUNC(ImportTest)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ExportTest", AMXO_FUNC(ExportTest)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "Restore", AMXO_FUNC(_Restore)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "SetActionStatus", AMXO_FUNC(_SetActionStatus)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "AddBackupFile", AMXO_FUNC(_AddBackupFile)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "pcm_backup_file_type_changed",
                                            AMXO_FUNC(_pcm_backup_file_type_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "max_number_dynamic_files_changed",
                                            AMXO_FUNC(_max_number_dynamic_files_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "backup_file_added",
                                            AMXO_FUNC(_backup_file_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "backup_file_removed",
                                            AMXO_FUNC(_backup_file_removed)), 0);


    assert_int_equal(amxo_parser_parse_file(parser, "../common/pcm-manager_test.odl", root_obj), 0);
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    assert_int_equal(amxo_connection_add(parser,
                                         amxb_get_fd(bus_ctx),
                                         connection_read,
                                         "dummy:/tmp/dummy.sock",
                                         AMXO_BUS,
                                         bus_ctx)
                     , 0);
    assert_int_equal(amxb_register(bus_ctx, dm), 0);


    helper->dm = dm;
    helper->parser = parser;
    _pcm_main(AMXO_START, helper->dm, helper->parser);

}

void pcm_test_helper_teardown(pcm_test_helper* helper) {
    _pcm_main(AMXO_STOP, helper->dm, helper->parser);
    amxo_resolver_import_close_all();
    assert_int_equal(test_unregister_dummy_be(), 0);
    amxp_signal_delete(&signal_handler);
    amxd_dm_delete(&(helper->dm));
    amxo_parser_delete(&(helper->parser));
}

void pcm_handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

void pcm_time_read_sigalrm(void) {
    sigset_t mask;
    int sfd;
    struct signalfd_siginfo fdsi;
    ssize_t s;

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);

    sfd = signalfd(-1, &mask, 0);
    s = read(sfd, &fdsi, sizeof(struct signalfd_siginfo));
    assert_int_equal(s, sizeof(struct signalfd_siginfo));
    if(fdsi.ssi_signo == SIGALRM) {
        printf("Got SIGALRM\n");
    } else {
        printf("Read unexpected signal\n");
    }
}

void pcm_wait_for_timer_event(void) {
    pcm_time_read_sigalrm();
    amxp_timers_calculate();
    amxp_timers_check();
}
static amxd_status_t ImportTest(UNUSED amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    amxc_var_dump(args, STDERR_FILENO);
    assert_non_null(args);
    assert_string_equal(GETP_CHAR(args, "content.Name"), "RestoreName");
    return amxd_status_ok;
}

static amxd_status_t ExportTest(UNUSED amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                UNUSED amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    return amxd_status_ok;
}

int __wrap_amxo_parser_save_object(UNUSED amxo_parser_t* pctx,
                                   UNUSED const char* filename,
                                   UNUSED amxd_object_t* object,
                                   UNUSED bool append) {
    return 0;
}

amxb_bus_ctx_t* __wrap_amxb_be_who_has(const char* object_path) {
    if((object_path != NULL) && (strcmp(object_path, "DeviceInfo.") == 0)) {
        ctx = (amxb_bus_ctx_t*) calloc(1, sizeof(amxb_bus_ctx_t));
        return ctx;
    } else {
        return __real_amxb_be_who_has(object_path);
    }
}

int __wrap_amxb_set(amxb_bus_ctx_t* const bus_ctx,
                    const char* object,
                    amxc_var_t* values,
                    amxc_var_t* ret,
                    int timeout) {
    return 0;
}