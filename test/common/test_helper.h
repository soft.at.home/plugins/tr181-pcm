/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc_variant.h>
#include <amxc/amxc_lqueue.h>
#include <amxc/amxc_variant_type.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_dm.h>
#include <amxc/amxc_rbuffer.h>
#include <amxc/amxc_astack.h>
#include <amxc/amxc_lstack.h>
#include <amxo/amxo.h>
#include <amxp/amxp_slot.h>
#include <amxb/amxb.h>
#include <amxb/amxb_be.h>
#include <amxb/amxb_register.h>

#include <setjmp.h>
#include <archive.h>
#include <archive_entry.h>
#include <cmocka.h>

#include "dummy_backend.h"

typedef struct {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
} pcm_test_helper;

typedef enum {
    success = 0,            /*Mock Function to success*/
    failed,                 /*Mock Function to failed*/
    called                  /*Call the real function*/
} func_return_t;

void pcm_test_helper_setup(pcm_test_helper* helper);
void pcm_test_helper_teardown(pcm_test_helper* helper);
void pcm_handle_events(void);
void pcm_time_read_sigalrm(void);
void pcm_wait_for_timer_event(void);
void set_func_return_value(func_return_t value);
void set_tar_append_to_fail(bool value);
void set_tar_extract_to_fail(bool value);
int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 amxc_var_t* ret);
int __wrap_amxo_parser_save_object(amxo_parser_t* pctx,
                                   const char* filename,
                                   amxd_object_t* object,
                                   bool append);

int __wrap_archive_write_header(struct archive* a, struct archive_entry* e);
int __wrap_archive_read_data_into_fd(struct archive* a, int fd);
bool __wrap_pcm_tar_create(const char* path, const char* filename);
bool __wrap_pcm_tar_extract(const char* path, const char* filename);
amxb_bus_ctx_t* __wrap_amxb_be_who_has(const char* object_path);
int __wrap_amxb_set(amxb_bus_ctx_t* const bus_ctx,
                    const char* object,
                    amxc_var_t* values,
                    amxc_var_t* ret,
                    int timeout);
void pcm_clean_ctx(void);
