/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <setjmp.h>
#include <stdlib.h>
#include <cmocka.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>


#include "fs_utils.h"
#include "dm_pcm_crypt.h"
#include "test_pcm_crypt.h"

#define BASE64_KEY_AES_128 "ZmFrZV9wcml2YXRlX2tleQ=="
#define BASE64_KEY_AES_192 "cGNtS0VZc29mdGF0aG9tZV9BRVMtMTky"
#define BASE64_KEY_AES_256 "cGNtX2Jhc2U2NGtleV9zb2Z0YXRob21lX0FFU18yNTY="

#define TEST_ROOT_DIR "tmp/crypt_dir"
#define REMOVE_ROOT "rm -rf "TEST_ROOT_DIR
#define REMOVE_FILE "rm "TEST_ROOT_DIR
#define TEST_FILES TEST_ROOT_DIR "/backup{001..003}.json"


#define CREATE_INITIAL_TREE "mkdir -p "TEST_ROOT_DIR
#define CREATE_PCM_USR_FILE "mkdir -p "PCM_USR_BACKUP
#define CREATE_FILES "touch " TEST_FILES
#define ADD_CONTENT "echo '{\"KEY_1 \": \"Value_1 \", \"KEY_2 \": true, \"KEY_3 \": 10}' > "
#define EXPECTED_CONTENT "{\"KEY_1 \": \"Value_1 \", \"KEY_2 \": true, \"KEY_3 \": 10}\n"

static bool get_file_content(const char* file, char** content);

int test_setup(UNUSED void** state) {
    //ensure all clean
    amxc_llist_t* files = NULL;

    assert_int_equal(0, system(REMOVE_ROOT));
    assert_int_equal(0, system(CREATE_INITIAL_TREE));
    assert_true(directory_exist(TEST_ROOT_DIR));
    assert_int_equal(0, system(CREATE_FILES));

    files = files_from_path(TEST_ROOT_DIR);
    assert_non_null(files);

    amxc_llist_for_each(iter, files) {
        amxc_string_t* file = amxc_container_of(iter, amxc_string_t, it);
        const char* file_str = amxc_string_get(file, 0);
        char* command = malloc(strlen(ADD_CONTENT) + strlen(TEST_ROOT_DIR) + strlen(file_str) + 2);
        strcpy(command, ADD_CONTENT);
        strcat(command, TEST_ROOT_DIR);
        strcat(command, "/");
        strcat(command, file_str);
        assert_int_equal(0, system(command));
        free(command);
    }
    amxc_llist_delete(&files, amxc_string_list_it_free);

    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_true(directory_exist(TEST_ROOT_DIR));
    assert_int_equal(0, system(REMOVE_ROOT));
    return 0;
}

void test_cannot_encrypt_with_wrong_key(void** state) {
    //The size of the decoded key must be 16 24 or 32 bytes
    const char* invalid_key = "cGNtX2Jhc2U2NGtleQ=="; // 13 bytes

    assert_true(directory_exist(TEST_ROOT_DIR));
    assert_false(pcm_encrypt_dir(TEST_ROOT_DIR, invalid_key));
}

void test_encrypt_decrypt_KEY_128(UNUSED void** state) {
    amxc_llist_t* files = NULL;
    size_t file_size;

    assert_true(directory_exist(TEST_ROOT_DIR));
    assert_true(pcm_encrypt_dir(TEST_ROOT_DIR, BASE64_KEY_AES_128));

    // GET ENCRYPTED FILES
    files = files_from_path(TEST_ROOT_DIR);
    assert_non_null(files);
    assert_int_equal(amxc_llist_size(files), 3);

    // COMPARE ENCRYPTED FILES WITH THE EXPECTED CONTENT
    amxc_llist_for_each(iter, files) {
        char* file_content = NULL;
        amxc_string_t* file = amxc_container_of(iter, amxc_string_t, it);
        amxc_string_prependf(file, "%s/", TEST_ROOT_DIR);

        assert_true(get_file_content(amxc_string_get(file, 0), &file_content));
        assert_true(strcmp(file_content, EXPECTED_CONTENT));

        free(file_content);
    }
    amxc_llist_delete(&files, amxc_string_list_it_free);

    assert_true(pcm_decrypt_dir(TEST_ROOT_DIR, BASE64_KEY_AES_128));

    // GET DECRYPTED FILES
    files = files_from_path(TEST_ROOT_DIR);
    assert_non_null(files);
    assert_int_equal(amxc_llist_size(files), 3);

    // COMPARE DECRYPTED FILES WITH THE EXPECTED CONTENT
    amxc_llist_for_each(iter, files) {
        char* file_content = NULL;
        amxc_string_t* file = amxc_container_of(iter, amxc_string_t, it);
        amxc_string_prependf(file, "%s/", TEST_ROOT_DIR);

        assert_true(get_file_content(amxc_string_get(file, 0), &file_content));
        assert_false(strcmp(file_content, EXPECTED_CONTENT));

        free(file_content);
    }
    amxc_llist_delete(&files, amxc_string_list_it_free);

}

void test_encrypt_decrypt_KEY_192(UNUSED void** state) {
    amxc_llist_t* files = NULL;
    size_t file_size;

    assert_true(directory_exist(TEST_ROOT_DIR));
    assert_true(pcm_encrypt_dir(TEST_ROOT_DIR, BASE64_KEY_AES_192));

    // GET ENCRYPTED FILES
    files = files_from_path(TEST_ROOT_DIR);
    assert_non_null(files);
    assert_int_equal(amxc_llist_size(files), 3);

    // COMPARE ENCRYPTED FILES WITH THE EXPECTED CONTENT
    amxc_llist_for_each(iter, files) {
        char* file_content = NULL;
        amxc_string_t* file = amxc_container_of(iter, amxc_string_t, it);
        amxc_string_prependf(file, "%s/", TEST_ROOT_DIR);

        assert_true(get_file_content(amxc_string_get(file, 0), &file_content));
        assert_true(strcmp(file_content, EXPECTED_CONTENT));

        free(file_content);
    }
    amxc_llist_delete(&files, amxc_string_list_it_free);

    assert_true(pcm_decrypt_dir(TEST_ROOT_DIR, BASE64_KEY_AES_192));

    // GET DECRYPTED FILES
    files = files_from_path(TEST_ROOT_DIR);
    assert_non_null(files);
    assert_int_equal(amxc_llist_size(files), 3);

    // COMPARE DECRYPTED FILES WITH THE EXPECTED CONTENT
    amxc_llist_for_each(iter, files) {
        char* file_content = NULL;
        amxc_string_t* file = amxc_container_of(iter, amxc_string_t, it);
        amxc_string_prependf(file, "%s/", TEST_ROOT_DIR);

        assert_true(get_file_content(amxc_string_get(file, 0), &file_content));
        assert_false(strcmp(file_content, EXPECTED_CONTENT));

        free(file_content);
    }
    amxc_llist_delete(&files, amxc_string_list_it_free);

}

void test_encrypt_decrypt_KEY_256(UNUSED void** state) {
    amxc_llist_t* files = NULL;
    size_t file_size;

    assert_true(directory_exist(TEST_ROOT_DIR));
    assert_true(pcm_encrypt_dir(TEST_ROOT_DIR, BASE64_KEY_AES_256));

    // GET ENCRYPTED FILES
    files = files_from_path(TEST_ROOT_DIR);
    assert_non_null(files);
    assert_int_equal(amxc_llist_size(files), 3);

    // COMPARE ENCRYPTED FILES WITH THE EXPECTED CONTENT
    amxc_llist_for_each(iter, files) {
        char* file_content = NULL;
        amxc_string_t* file = amxc_container_of(iter, amxc_string_t, it);
        amxc_string_prependf(file, "%s/", TEST_ROOT_DIR);

        assert_true(get_file_content(amxc_string_get(file, 0), &file_content));
        assert_true(strcmp(file_content, EXPECTED_CONTENT));

        free(file_content);
    }
    amxc_llist_delete(&files, amxc_string_list_it_free);

    assert_true(pcm_decrypt_dir(TEST_ROOT_DIR, BASE64_KEY_AES_256));

    // GET DECRYPTED FILES
    files = files_from_path(TEST_ROOT_DIR);
    assert_non_null(files);
    assert_int_equal(amxc_llist_size(files), 3);

    // COMPARE DECRYPTED FILES WITH THE EXPECTED CONTENT
    amxc_llist_for_each(iter, files) {
        char* file_content = NULL;
        amxc_string_t* file = amxc_container_of(iter, amxc_string_t, it);
        amxc_string_prependf(file, "%s/", TEST_ROOT_DIR);

        assert_true(get_file_content(amxc_string_get(file, 0), &file_content));
        assert_false(strcmp(file_content, EXPECTED_CONTENT));

        free(file_content);
    }
    amxc_llist_delete(&files, amxc_string_list_it_free);

}

void test_cannot_encrypt_twice(void** state) {
    amxc_llist_t* files = NULL;
    size_t file_size;

    assert_true(directory_exist(TEST_ROOT_DIR));
    assert_true(pcm_encrypt_dir(TEST_ROOT_DIR, BASE64_KEY_AES_128));
    assert_false(pcm_encrypt_dir(TEST_ROOT_DIR, BASE64_KEY_AES_128));

}

void test_cannot_decrypt_twice(void** state) {
    amxc_llist_t* files = NULL;
    size_t file_size;

    assert_true(directory_exist(TEST_ROOT_DIR));
    assert_true(pcm_decrypt_dir(TEST_ROOT_DIR, BASE64_KEY_AES_128));
    assert_false(pcm_decrypt_dir(TEST_ROOT_DIR, BASE64_KEY_AES_128));
}

static bool get_file_content(const char* file, char** content) {
    bool rv = false;
    FILE* fd = NULL;
    long file_size = 0;

    fd = fopen(file, "r");
    assert_non_null(fd);
    fseek(fd, 0, SEEK_END);
    file_size = ftell(fd);
    rewind(fd);
    *content = (char*) malloc(file_size + 1);

    assert_non_null(*content);
    assert_int_equal(fread(*content, sizeof(char), file_size, fd), file_size);


    fclose(fd);
    rv = true;
    return rv;
}

