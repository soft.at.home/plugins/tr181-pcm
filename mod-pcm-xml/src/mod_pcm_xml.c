/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <stdio.h>


#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>
#include <libxml/parser.h>

#include <amxc/amxc_variant_type.h>

#include "mod_pcm_xml.h"

#ifdef ME
#undef ME
#endif

#define ME "pcm_xml"
#define BEAUTIFY_NUMBER_OF_SPACES 4

typedef struct  {
    amxm_shared_object_t* so;
} mod_pcm_xml_t;

struct xml_beautify {
    bool enable;
    unsigned int indent_len;
};
static struct xml_beautify beautify;
static mod_pcm_xml_t pcm;
static amxd_status_t pcm_write_beautify_stream(FILE* out_file, const amxc_var_t* const content);
static amxd_status_t pcm_write_stream(FILE* out_file, const amxc_var_t* const content);
static void xml_writeStreamInternal(FILE* f, const amxc_var_t* const variant);
static void xml_readStreamInternal(xmlNode* node, amxc_var_t* backup);
static int is_leaf(xmlNode* node);

int pcm_store_xml(UNUSED const char* function_name,
                  amxc_var_t* args,
                  UNUSED amxc_var_t* ret) {
    int rc = amxd_status_unknown_error;
    const char* filename = NULL;
    amxc_var_t* content = NULL;
    amxc_var_t data;
    bool beautify_enable = false;
    FILE* stream = NULL;
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    when_null(args, exit);

    filename = GET_CHAR(args, "name");
    content = GET_ARG(args, "content");
    beautify_enable = GET_BOOL(args, "beautify");

    when_str_empty(filename, exit);
    when_null(content, exit);

    amxc_var_add_key(amxc_htable_t, &data, "data", amxc_var_constcast(amxc_htable_t, content));

    stream = fopen(filename, "w");
    when_null_trace(stream, exit, ERROR, "Cannot open file %s", filename);


    SAH_TRACEZ_INFO(ME, "Store backup %s", filename);
    rc = (beautify_enable) ? pcm_write_beautify_stream(stream, &data) : pcm_write_stream(stream, &data);

exit:
    amxc_var_clean(&data);
    if(stream) {
        fflush(stream);
        fclose(stream);
    }
    return rc;
}

int pcm_load_xml(UNUSED const char* function_name,
                 amxc_var_t* args,
                 amxc_var_t* ret) {
    int rc = amxd_status_unknown_error;
    const char* filename = NULL;
    xmlDocPtr doc = NULL;
    xmlNode* root_node = NULL;

    when_null(args, exit);
    when_null(ret, exit);

    filename = GET_CHAR(args, "name");
    when_str_empty(filename, exit);

    doc = xmlReadFile(filename, NULL, 0);
    when_null_trace(doc, exit, ERROR, "Failed to parse %s\n", filename);
    root_node = xmlDocGetRootElement(doc);

    SAH_TRACEZ_INFO(ME, "Read backup %s", filename);
    xml_readStreamInternal(root_node, ret);
    rc = amxd_status_ok;
exit:
    xmlFreeDoc(doc);
    return rc;
}

static AMXM_CONSTRUCTOR pcm_xml_start(void) {
    amxm_module_t* mod = NULL;
    pcm.so = amxm_so_get_current();

    amxm_module_register(&mod, pcm.so, MOD_PCM_XML);
    amxm_module_add_function(mod, "load-backup", pcm_load_xml);
    amxm_module_add_function(mod, "store-backup", pcm_store_xml);

    return 0;
}

static AMXM_DESTRUCTOR pcm_xml_stop(void) {
    return 0;
}

static amxd_status_t pcm_write_beautify_stream(FILE* out_file, const amxc_var_t* const content) {
    amxd_status_t rc = amxd_status_unknown_error;
    beautify.enable = true;
    rc = pcm_write_stream(out_file, content);
    beautify.enable = false;
    return rc;
}

static amxd_status_t pcm_write_stream(FILE* out_file, const amxc_var_t* const content) {
    amxd_status_t rc = amxd_status_unknown_error;

    when_null_trace(out_file, exit, ERROR, "Not a valid file pointer");
    when_null_trace(content, exit, ERROR, "No data to store");

    if((content->type_id != AMXC_VAR_ID_HTABLE) && (content->type_id != AMXC_VAR_ID_LIST)) {
        fprintf(out_file, "null");
        goto exit;
    }

    fprintf(out_file, "<?xml version=\"1.0\" encoding=\"utf-8\"?>");

    if(beautify.enable) {
        fprintf(out_file, "\n");
    }
    beautify.indent_len = 0;
    xml_writeStreamInternal(out_file, content);
    rc = amxd_status_ok;

exit:
    return rc;
}

static void xml_writeStreamInternal(FILE* f, const amxc_var_t* const variant) {
    char* retval = NULL;
    amxc_var_t* sub_var = NULL;
    bool is_empty = false;

    when_null_trace(f, exit, ERROR, "Not a valid file pointer");
    when_null_trace(variant, exit, ERROR, "No data to store");

    switch(variant->type_id) {
    case AMXC_VAR_ID_CSTRING: {
        if(amxc_var_constcast(cstring_t, variant) == NULL) {
            fprintf(f, "null");
            break;
        }
        SAH_TRACEZ_INFO(ME, "Writing string %s", amxc_var_constcast(cstring_t, variant));
        /* escape chars */
        const char* c;
        for(c = amxc_var_constcast(cstring_t, variant); *c; c++) {
            switch(*c) {
            case '"': fprintf(f, "&quot;"); break;
            case '&': fprintf(f, "&amp;"); break;
            case '\'': fprintf(f, "&apos;"); break;
            case '<': fprintf(f, "&lt;"); break;
            case '>': fprintf(f, "&gt;"); break;
            default: {
                fputc(*c, f);
                break;
            }
            }
        }
    }
    break;
    case AMXC_VAR_ID_INT8:
    case AMXC_VAR_ID_INT16:
    case AMXC_VAR_ID_INT32:
    case AMXC_VAR_ID_INT64:
    case AMXC_VAR_ID_UINT8:
    case AMXC_VAR_ID_UINT16:
    case AMXC_VAR_ID_UINT32:
    case AMXC_VAR_ID_UINT64:
    case AMXC_VAR_ID_DOUBLE:
    case AMXC_VAR_ID_FD:
    case AMXC_VAR_ID_TIMESTAMP:
        retval = amxc_var_dyncast(cstring_t, variant);
        SAH_TRACEZ_INFO(ME, "Writing %s %s", amxc_var_get_type_name_from_id(variant->type_id), retval);
        fprintf(f, "%s", retval);
        break;
    case AMXC_VAR_ID_BOOL:
        SAH_TRACEZ_INFO(ME, "Writing bool %d", GET_BOOL(variant, NULL));
        if(GET_BOOL(variant, NULL)) {
            fprintf(f, "true");
        } else {
            fprintf(f, "false");
        }
        break;
    case AMXC_VAR_ID_LIST: {
        SAH_TRACEZ_INFO(ME, "Writing array");
        amxc_llist_for_each(it, amxc_var_constcast(amxc_llist_t, variant)) {
            beautify.indent_len++;
            xml_writeStreamInternal(f, amxc_llist_it_get_data(it, amxc_var_t, lit));
            if(amxc_llist_it_get_next(it)) {
                fprintf(f, ",");
            }
        }
    }
    break;
    case AMXC_VAR_ID_HTABLE: {
        SAH_TRACEZ_INFO(ME, "Writing htable");

        amxc_htable_for_each(it, amxc_var_constcast(amxc_htable_t, variant)) {
            SAH_TRACEZ_INFO(ME, "Writing map %s", it->key);
            if(beautify.enable && (beautify.indent_len > 0)) {
                fprintf(f, "%*c<", beautify.indent_len * BEAUTIFY_NUMBER_OF_SPACES, ' ');
            } else {
                fprintf(f, "<");
            }
            fprintf(f, "%s", it->key);
            fprintf(f, ">");

            sub_var = amxc_htable_it_get_data(it, amxc_var_t, hit);
            is_empty = amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, sub_var));
            if(sub_var && (sub_var->type_id == AMXC_VAR_ID_HTABLE) && beautify.enable && !is_empty) {

                fprintf(f, "\n");
            }

            beautify.indent_len++;
            xml_writeStreamInternal(f, sub_var);

            SAH_TRACEZ_INFO(ME, "Stopping map %s", it->key);
            if(beautify.enable && (beautify.indent_len > 0) &&
               (sub_var->type_id == AMXC_VAR_ID_HTABLE) && !is_empty) {
                fprintf(f, "%*c</", beautify.indent_len * BEAUTIFY_NUMBER_OF_SPACES, ' ');
            } else {
                fprintf(f, "</");
            }
            fprintf(f, "%s", it->key);
            if(beautify.enable) {
                fprintf(f, ">\n");
            } else {
                fprintf(f, ">");
            }
        }
        break;
    }
    default:
        fprintf(f, "null");
        break;
    }

exit:
    free(retval);
    beautify.indent_len--;
    return;
}

static void xml_readStreamInternal(xmlNode* node, amxc_var_t* backup) {
    amxc_var_t* data = NULL;
    char* content = NULL;

    when_null(node, exit);
    when_null(backup, exit);

    while(node) {
        if(node->type == XML_ELEMENT_NODE) {
            content = (char*) xmlNodeGetContent(node);
            if(is_leaf(node) && (strcmp(content, "") != 0)) {
                data = amxc_var_add_key(cstring_t, backup, (char*) node->name, content);
            } else {
                data = amxc_var_add_key(amxc_htable_t, backup, (char*) node->name, NULL);
            }
            free(content);
        }
        xml_readStreamInternal(node->children, data);
        node = node->next;
    }

exit:
    return;
}

static int is_leaf(xmlNode* node) {
    bool ret = 0;
    xmlNode* child = NULL;
    when_null(node, exit);

    child = node->children;
    while(child) {
        if(child->type == XML_ELEMENT_NODE) {
            goto exit;
        }
        child = child->next;
    }
    ret = 1;
exit:
    return ret;
}


#undef ME