/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxb/amxb_types.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "test_module.h"
#include "mod_pcm_json.h"

static amxd_dm_t dm;
static amxo_parser_t parser;

static void test_prepare_content(amxc_var_t* args, const char* filename);
static void test_fill_content(amxc_var_t* test_content);

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    handle_events();
    return 0;
}

int test_teardown(UNUSED void** state) {
    amxm_close_all();

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_can_load_module(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, "target_module", "../modules_under_test/target_module.so"), 0);
    handle_events();
}

void test_can_store_variant(UNUSED void** state) {
    amxc_var_t args;
    const char* test_output_json = "exported_variant.json";

    test_prepare_content(&args, test_output_json);

    assert_int_equal(pcm_store_json(NULL, &args, NULL), 0);

    amxc_var_clean(&args);
}

void test_try_store_variant_without_content(UNUSED void** state) {
    amxc_var_t args;
    const char* test_output_json = NULL;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", test_output_json);

    assert_int_not_equal(pcm_store_json(NULL, &args, NULL), 0);

    amxc_var_clean(&args);
}

void test_try_store_variant_without_filename(UNUSED void** state) {
    amxc_var_t args;
    const char* test_output_json = NULL;

    test_prepare_content(&args, test_output_json);
    assert_int_not_equal(pcm_store_json(NULL, &args, NULL), 0);

    amxc_var_clean(&args);
}

void test_try_load_variant_without_filename(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    const char* test_input_json = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", test_input_json);

    assert_int_not_equal(pcm_load_json(NULL, &args, &ret), 0);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);
}

void test_can_load_variant(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t reference;
    amxc_var_t* content = NULL;

    const char* test_input_json = "input_variant.json";

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_init(&reference);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&ret, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&reference, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", test_input_json);

    assert_int_equal(0, pcm_load_json(NULL, &args, &ret));

    test_fill_content(&reference);
    content = amxc_var_get_path(&ret, "data", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(content);

    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, content)),
                     amxc_htable_size(amxc_var_constcast(amxc_htable_t, &reference)));

    assert_string_equal(GETP_CHAR(content, "KEY_1"), GETP_CHAR(content, "KEY_1"));
    assert_true(GETP_BOOL(content, "KEY_2") == GETP_BOOL(content, "KEY_2"));
    assert_int_equal(GETP_UINT32(content, "KEY_3"), GETP_UINT32(content, "KEY_3"));

    amxc_var_clean(&ret);
    amxc_var_clean(&reference);
    amxc_var_clean(&args);
}

static void test_prepare_content(amxc_var_t* args, const char* filename) {
    amxc_var_t* test_content = NULL;

    amxc_var_init(args);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, "name", filename);
    test_content = amxc_var_add_key(amxc_htable_t, args, "content", NULL);

    test_fill_content(test_content);
}

static void test_fill_content(amxc_var_t* test_content) {
    amxc_var_add_key(cstring_t, test_content, "KEY_1", "Value_1");
    amxc_var_add_key(bool, test_content, "KEY_2", true);
    amxc_var_add_key(uint32_t, test_content, "KEY_3", 10);
}
