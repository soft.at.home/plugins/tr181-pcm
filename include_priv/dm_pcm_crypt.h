/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __DM_PCM_CRYPT_H__
#define __DM_PCM_CRYPT_H__

#include <stdbool.h>
#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Locates all the files in the specified folder and encrypts each file using the Advanced Encryption Standard (AES)
 * algorithm with the specified key. The encrypted files are stored in the same folder with a suffix ".encrypt" added
 * to the original file name.
 *
 * @param filename filename of the file to encrypt
 * @param base64_key key for encryption in base64 format.
 *
 * @return true on success, false on failure;
 */
bool pcm_encrypt_dir(const char* path, const char* base64_key);

/**
 * Locates all the files in the specified folder and decrypts each file using the Advanced Encryption Standard (AES)
 * algorithm with the specified key. The decrypted files are stored in the same folder removing the suffix ".encrypt"
 * from the original file name.
 *
 * @param filename filename of the file to decrypt
 * @param base64_key key for decryption in base64 format.
 *
 * @return true on success, false on failure;
 */
bool pcm_decrypt_dir(const char* path, const char* base64_key);

/**
 * Encrypts the content of the file using the pcm_encrypt method. The resulting encrypted data is stored in the
 * output argument "out," with the size of the encrypted data stored in the outputargument "outsize."
 *
 * @param filename filename of the file to encrypt
 * @param base64_key key for encryption in base64 format.
 * @param out[out] buffer containing the encrypted data (allocated by this function);
 * @param outsize[out] number of bytes in encrypted message
 *
 * @return true on success, false on failure; on success <out> should be freed
 */
bool pcm_encrypt_file(const char* filename, const char* base64_key,
                      unsigned char** out, size_t* outsize);
/**
 * Decrypts the content of the file using the pcm_decrypt method. The resulting decrypted data is stored in the
 * output argument "out," with the size of the decrypted data stored in the outputargument "outsize."
 *
 * @param filename filename of the file to decrypt
 * @param base64_key key for decryption in base64 format.
 * @param out[out] buffer containing the decrypted data (allocated by this function);
 * @param outsize[out] number of bytes in decrypted message
 *
 * @return true on success, false on failure; on success <out> should be freed
 */
bool pcm_decrypt_file(const char* filename, const char* base64_key,
                      unsigned char** out, size_t* outsize);


/**
 * Encrypts the content using the Advanced Encryption Standard (AES) algorithm with
 * the specified key in base64 format. The resulting encrypted data is stored in the
 * output argument "out," with the size of the encrypted data stored in the output
 * argument "outsize."
 *
 * @param in the data to encrypt
 * @param insize number of bytes to encrypt
 * @param base64_key key for encryption in base64 format.
 * @param out[out] buffer containing the ecrypted data (allocated by this function);
 * @param outsize[out] number of bytes in ecrypted message
 *
 * @return true on success, false on failure; on success <out> should be freed
 */
bool pcm_encrypt(unsigned char* in, size_t insize, const char* base64_key,
                 unsigned char** out, size_t* outsize);

/**
 * Decrypts the content using the Advanced Encryption Standard (AES) algorithm with
 * the specified key in base64 format. The resulting decrypted data is stored in the
 * output argument "out," with the size of the decrypted data stored in the output
 * argument "outsize."
 *
 * @param in the data to decrypt
 * @param insize number of bytes to decrypt
 * @param base64_key key for decryption in base64 format.
 * @param out[out] buffer containing the decrypted data (allocated by this function);
 * @param outsize[out] number of bytes in decrypted message, including trailing
 *
 * @return true on success, false on failure; on success <out> should be freed
 */
bool pcm_decrypt(unsigned char* in, size_t insize, const char* base64_key,
                 unsigned char** out, size_t* outsize);


#ifdef __cplusplus
}
#endif

#endif // __DM_PCM_CRYPT_H__
