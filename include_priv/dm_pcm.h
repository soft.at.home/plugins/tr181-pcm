/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__DM_PCM_H__)
#define __DM_PCM_H__

#define CHECK_SCHEDULED_SERVICES 2000

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <amxb/amxb.h>

#include <amxo/amxo.h>
#include <amxo/amxo_save.h>
#include <debug/sahtrace.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct  {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    amxc_string_t* root_object;
    amxp_timer_t* scheduler;
} pcm_app_t;

typedef enum {
    pcm_filetype_json = 0,            /*JSON filetype*/
    pcm_filetype_xml,                 /*XML filetype*/
} pcm_filetype_t;

typedef enum {
    Disable = 0,
    Enable = 1,
    ForceEnable = 2
} pcm_auto_sync_t;

#define pcm_mod_max    (pcm_filetype_xml + 1)      /* Last pcm_filetype_t plus one */
#define STR_EMPTY(x) (x) == NULL || (x)[0] == '\0'

const char* pcm_root_object(void);
amxd_dm_t* PRIVATE pcm_get_dm(void);
amxo_parser_t* PRIVATE pcm_get_parser(void);
const char* PRIVATE pcm_get_store_path(const char* type);
const char* PRIVATE pcm_get_modules_path(void);
amxc_llist_it_t* PRIVATE add_scheduler_string(const char* text);
const char* PRIVATE pcm_get_ctrl(void);
void PRIVATE pcm_backup_file_path(const char* service, amxc_var_t* store_args, const char* backup_type);
amxd_object_t* PRIVATE pcm_get_service(const char* name);
pcm_filetype_t PRIVATE pcm_get_file_type(void);
amxc_string_t* pcm_get_backup_file(const char* name);

int _pcm_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser);

amxd_status_t _check_is_empty_or_in(amxd_object_t* object,
                                    amxd_param_t* param,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv);

#ifdef __cplusplus
}
#endif

#endif // __DM_PCM_H__
